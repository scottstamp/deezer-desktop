const gulp = require('gulp');
const clean = require('gulp-clean');

gulp.task('copy:assets', () => {
	return gulp
		.src([
			'./slash/dist/electron/**/*',
			'!./slash/dist/electron/**/*.php',
			'!./slash/dist/electron/_statics/**/*.map.json',
			'!./slash/dist/electron/_statics/js/pages/**/*',
			'!./slash/dist/electron/_statics/js/pages',
			'!./slash/dist/electron/_statics/fonts/bomanager',
			'!./slash/dist/electron/_statics/js/app-!(web).*.js',
			'!./slash/dist/electron/_statics/css/sass_c/pages/**/*',
			'!./slash/dist/electron/_statics/css/sass_c/pages',
			'!./slash/dist/electron/_statics/css/sass_c/app-!(web).*.css',
			'!./slash/dist/electron/_statics/images/affiliate/**/*',
			'!./slash/dist/electron/_statics/images/affiliate',
			'!./slash/dist/electron/_statics/images/backstage/**/*',
			'!./slash/dist/electron/_statics/images/backstage',
			'!./slash/dist/electron/_statics/images/bomanager/**/*',
			'!./slash/dist/electron/_statics/images/bomanager',
			'!./slash/dist/electron/_statics/images/business/**/*',
			'!./slash/dist/electron/_statics/images/business',
			'!./slash/dist/electron/_statics/images/developers/**/*',
			'!./slash/dist/electron/_statics/images/developers',
			'!./slash/dist/electron/_statics/images/mobile/**/*',
			'!./slash/dist/electron/_statics/images/mobile',
			'!./slash/dist/electron/_statics/images/pages/**/*',
			'!./slash/dist/electron/_statics/images/pages',
			'!./slash/dist/electron/_statics/images/plugins/**/*',
			'!./slash/dist/electron/_statics/images/plugins',
			'!./slash/dist/electron/_statics/images/partners/**/*',
			'!./slash/dist/electron/_statics/images/partners',
			'!./slash/dist/electron/_statics/images/widgets/**/*',
			'!./slash/dist/electron/_statics/images/widgets',
			'!./slash/dist/electron/_statics/images/unlogged/**/*',
			'!./slash/dist/electron/_statics/images/unlogged',
			'!./slash/dist/electron/_statics/swf/**/*',
			'!./slash/dist/electron/_statics/swf',
			'!./slash/dist/electron/_statics/css/bo/**/*',
			'!./slash/dist/electron/_statics/css/bo',
			'!./slash/dist/electron/_statics/css/bocss/**/*',
			'!./slash/dist/electron/_statics/css/bocss',
			'!./slash/dist/electron/_statics/css/bomanager/**/*',
			'!./slash/dist/electron/_statics/css/bomanager',
			'!./slash/dist/electron/_statics/css/d4a/**/*',
			'!./slash/dist/electron/_statics/css/d4a',
			'!./slash/dist/electron/_statics/css/developers/**/*',
			'!./slash/dist/electron/_statics/css/developers',
			'!./slash/dist/electron/_statics/css/instantwin/**/*',
			'!./slash/dist/electron/_statics/css/instantwin',
			'!./slash/dist/electron/_statics/css/partners/**/*',
			'!./slash/dist/electron/_statics/css/partners',
			'!./slash/dist/electron/_statics/css/sass/**/*',
			'!./slash/dist/electron/_statics/css/sass'
		], {base: './slash/dist/electron'})
		.pipe(gulp.dest('./app/assets'));
});

gulp.task('clean:assets', () =>  {
	return gulp
		.src('app/assets', {read: false})
		.pipe(clean());
});
