require('./assets.js');

const path = require('path');
const gulp = require('gulp');
const babel = require('gulp-babel');
const clean = require('gulp-clean');
const merge = require('merge-stream');
const eslint = require('gulp-eslint');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const filter = require('gulp-filter');
const uglify = require('gulp-uglify');
const htmlmin = require('gulp-htmlmin');
const gulpSequence = require('gulp-sequence').use(gulp);

const electron = require('electron-connect').server.create({
	logLevel: 0,
	spawnOpt: {
		env: {
			ELECTRON_CONNECT: 'on',
			DEEZER_DESKTOP_ENV: 'dev',
			DZ_FORCE_TRIFORCE: process.env.DZ_FORCE_TRIFORCE || 'no',
			DZ_DEVTOOLS: process.env.DZ_DEVTOOLS || 'no',
			DZ_DISABLE_UPDATE: process.env.DZ_DISABLE_UPDATE || 'no',
			DZ_DEV_ENV: process.env.DZ_DEV_ENV || ''
		}
	}
});

gulp.task('clean:local', function () {
	return gulp.src([
		'app/js',
		'app/html'
	],{
		read: false
	}).pipe(clean());
});

gulp.task('electron-connect-stop', done => {
	console.log('TRYING TO STOP', electron.electronState);
	if (electron.electronState === 'started' ||
		electron.electronState === 'restarted') {
		electron.stop((status) => {
			console.log('Stopped: ', status);
			done();
		});
	} else {
		done();
	}
});

gulp.task('electron-connect-restart', done => {

	if (
		electron.electronState === 'started' ||
		electron.electronState === 'restarted' ||
		electron.electronState === 'restarting'
	) {
		// Double restart, because we have to ..
		electron.restart((statusOne) => {
			if (statusOne === 'restarted') {
				done();
			}
			if (electron.electronState === 'restarting') {
				electron.restart((statusTwo) => {
					if (statusTwo === 'restarted') {
						done();
					}
				});
			}
		});
	}
	if (electron.electronState === 'init' ||
		electron.electronState === 'stopped') {
		electron.start(() => {
			done();
		});
	}
});

gulp.task('compile:local', ['clean:local'], () => {
	const html = gulp.src(['src/html/*.html', 'src/html/**/*.html'])
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest('app/html'));

	const noExternals = filter(['src/**', '!src/js/externals/**/*', '!src/js/externals/*'], {restore: true});

	const js = gulp.src('src/js/**/*.js')
		.pipe(noExternals)
		.pipe(plumber({
			errorHandler: function(err) {
				// Cleanup of base path
				err.message = err.message.replace(path.dirname(path.normalize(__dirname)),'');
				notify.onError({
					title:    'Gulp Error',
					message:  'Error: <%= error.message %>',
					sound:    'Bottle'
				})(err);
			}
		}))
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(babel())
		.pipe(uglify())
		.pipe(noExternals.restore)
		.pipe(gulp.dest('app/js'));

	const json = gulp.src('src/js/**/*.json')
		.pipe(gulp.dest('app/js'));

	return merge(html, js, json);
});


gulp.task('prepareWatch', (cb) => {gulpSequence(
	'clean:assets',
	'copy:assets',
	'clean:local',
	'compile:local',
	'electron-connect-restart'
)(cb);});

gulp.task('doWatch', (cb) => {gulpSequence('electron-connect-stop', 'compile:local', 'electron-connect-restart')(cb);});

gulp.task('watch', ['prepareWatch'], () => {
	gulp.watch(['src/js/**/*.js', 'src/js/**/*.json', 'src/html/**/*.html'], ['doWatch']);
});
