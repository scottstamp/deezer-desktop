const gulp = require('gulp');
const path = require('path');
const fs = require('fs-extra');
const yaml = require('js-yaml');
const builder = require('electron-builder');
const childProcess = require('child_process');
const process = require('process');
const packageJson = require('../package.json');
const move = require('glob-move');
const through = require('through2');

const Platform = builder.Platform;
const Arch = builder.Arch;

process.env.NODE_ENV = 'prod';

const shouldMoveFileToVersionFolder = process.env.DZR_NO_DIST_MOVE_TO_VERSION !== 'true';

const productName = 'Deezer';
const shortProductName = 'DeezerDesktop';

const build = function() {
	const baseConfig = {
		appId: 'com.deezer.deezer-desktop',
		productName: productName,
		npmRebuild: false,
	};

	const macConfig = Object.assign({}, baseConfig, {
		icon: 'build/mac/app.icns',
		mac: {
			category: 'public.app-category.music',
			artifactName: shortProductName + '_${version}-mac.${ext}'
		},
		dmg: {
			artifactName: shortProductName + '_${version}.${ext}',
			icon: 'build/mac/dmg.icns',
			background: 'build/mac/background.tiff',
			iconSize: 128,
			contents: [
				{
					'x': 160,
					'y': 128
				}, {
					'x': 480,
					'y': 128,
					'type': 'link',
					'path': '/Applications'
				}
			],
			window: {
				x: 150,
				y: 150,
				width: 640,
				height: 480
			}
		},
		publish: [
			{
				provider: 'generic',
				url: 'https://e-cdns-content.dzcdn.net/builds/deezer-desktop/8cF2rAuKxLcU1oMDmCYm8Uiqe19Ql0HTySLssdzLkQ9ZWHuDTp2JBtQOvdrFzWPA/mac/x64/'
			}
		],
		afterPack: function() {
			return new Promise(function(resolve, reject) {
				// Ugly fix ... I'm sorry :)
				console.log('Re-making libdeezer.framework symlinks ...');
				const scriptPath = path.join(__dirname, '/../dist/mac/', productName + '.app', 'Contents/Resources/app.asar.unpacked/node_modules/node-deezer-native/scripts/', 'preinstall.js');
				let datProc = childProcess.fork(scriptPath);
				datProc.on('error', function(code, b) {
					console.log('Error symlinking', code, b);
					let err = code === 0
						? resolve()
						: reject(code);
				});

				datProc.on('exit', function(code) {
					console.log('Finished symliking');
					let err = code === 0
						? resolve()
						: reject(code);
				});

			});
		},
		files: [
			'**/*',
			'!deezer',
			'!slash',
			'!node_modules/node-deezer-native/${/*}',
			'node_modules/node-deezer-native/scripts/${/*}',
			'node_modules/node-deezer-native/index.js',
			'node_modules/node-deezer-native/package.json',
			'node_modules/node-deezer-native/build/Release/DeezerNative.node',
			'node_modules/node-deezer-native/deps/DeezerNativeSDK/Bins/Platforms/${os}/${arch}${/*}'
		]
	});

	const winConfig = Object.assign({}, baseConfig, {
		icon: 'build/win/app.ico',
		win: {
			publisherName: 'Deezer'
		},
		nsis: {
			installerIcon: 'build/win/app.ico',
			installerHeader: 'build/win/installerHeader.bmp',
			artifactName: shortProductName + 'Setup_${version}.${ext}',
			deleteAppDataOnUninstall: true
		},
		publish: [
			{
				provider: 'generic',
				url: 'https://e-cdns-content.dzcdn.net/builds/deezer-desktop/8cF2rAuKxLcU1oMDmCYm8Uiqe19Ql0HTySLssdzLkQ9ZWHuDTp2JBtQOvdrFzWPA/win32/x64/'
			}
		],
		extraResources: ['build/win/app.ico'],
		files: [
			'**/*',
			'!deezer',
			'!slash',
			'node_modules/node-deezer-native/index.js',
			'node_modules/node-deezer-native/package.json',
			'node_modules/node-deezer-native/build/Release/DeezerNative.node',
			'!node_modules/node-deezer-native/deps/DeezerNativeSDK/Bins/Platforms/!(${os})/${/*}',
			'!node_modules/node-deezer-native/deps/DeezerNativeSDK/Bins/Platforms/!(${os})/!(${arch})${/*}',
			'!node_modules/node-deezer-native/DeezerNative.cc',
			'!node_modules/node-deezer-native/**/*.gypi',
			'!node_modules/node-deezer-native/**/*.tlog',
			'!node_modules/node-deezer-native/**/*.h',
			'!node_modules/node-deezer-native/**/*.pdb',
			'!node_modules/node-deezer-native/**/*.lib',
			'!node_modules/node-deezer-native/**/*.obj',
			'!node_modules/node-deezer-native/**/*.exp',
			'!node_modules/node-deezer-native/**/*.map',
			'!node_modules/node-deezer-native/**/*.exp',
			'!node_modules/node-deezer-native/**/*.vcxproj*',
			'!node_modules/node-deezer-native/**/*.gyp'
		]
	});

	const linuxConfig = Object.assign({}, baseConfig, {
		target: 'AppImage',
		publish: [
			{
				provider: 'generic',
				url: 'https://e-cdns-content.dzcdn.net/builds/deezer-desktop/8cF2rAuKxLcU1oMDmCYm8Uiqe19Ql0HTySLssdzLkQ9ZWHuDTp2JBtQOvdrFzWPA/linux/x64/'
			}
		],
		files: [
			'**/*',
			'!deezer',
			'!slash',
			'node_modules/node-deezer-native/index.js',
			'node_modules/node-deezer-native/package.json',
			'node_modules/node-deezer-native/build/Release/DeezerNative.node',
			'!node_modules/node-deezer-native/deps/DeezerNativeSDK/Bins/Platforms/!(${os})/${/*}',
			'!node_modules/node-deezer-native/deps/DeezerNativeSDK/Bins/Platforms/!(${os})/!(${arch})${/*}',
			'!node_modules/node-deezer-native/DeezerNative.cc',
			'!node_modules/node-deezer-native/**/*.gypi',
			'!node_modules/node-deezer-native/**/*.tlog',
			'!node_modules/node-deezer-native/**/*.h',
			'!node_modules/node-deezer-native/**/*.pdb',
			'!node_modules/node-deezer-native/**/*.lib',
			'!node_modules/node-deezer-native/**/*.obj',
			'!node_modules/node-deezer-native/**/*.exp',
			'!node_modules/node-deezer-native/**/*.map',
			'!node_modules/node-deezer-native/**/*.exp',
			'!node_modules/node-deezer-native/**/*.vcxproj*',
			'!node_modules/node-deezer-native/**/*.gyp'
		]
	});

	let targets = false;
	let config = false;

	switch (process.platform) {
		case 'darwin':
			targets = Platform.MAC.createTarget();
			config = macConfig;
			break;
		case 'win32':
			targets = Platform.WINDOWS.createTarget(null, Arch.ia32);
			config = winConfig;
			break;
		case 'linux':
			targets = Platform.LINUX.createTarget();
			config = linuxConfig;
			break;
	}

	if (targets !== false && config !== false) {
		return builder.build({targets: targets, appDir: 'app', config: config, isTwoPackageJsonProjectLayoutUsed: false}).then(function() {
			let arch = 'x64';
			switch (process.arch) {
				case 'ia32':
				case 'x32':
				case 'x86':
					arch = 'x86';
					break;
				case 'x64':
					arch = 'x64';
					break;
				default:
					log.error(`Platform ${process.arch} is not known`);
			}
			console.log('Success Building for ', process.platform, ':', arch);

			switch (process.platform) {
				case 'darwin':
					{
						const distFolder = path.join(__dirname, '..', 'dist');

						const yamlPath = path.join(distFolder, 'latest-mac.yml');
						var yamlContent = yaml.safeLoad(fs.readFileSync(yamlPath, 'utf8'));
						yamlContent.path = ['artifact', process.platform, arch, packageJson.version].join('/');
						fs.writeFileSync(yamlPath, yaml.safeDump(yamlContent));

						const versionFolder = path.join(distFolder, packageJson.version);
						const pathToDelete = path.join(distFolder, 'mac');
						fs.remove(pathToDelete)
							.then(() => {
								if (shouldMoveFileToVersionFolder) {
									move(path.join(distFolder, '*'), versionFolder).then(() => {
										process.exit();
									});
								} else {
									process.exit();
								}
							});
					}
					break;
				case 'win32':
					{
						const distFolder = path.join(__dirname, '..', 'dist');

						const yamlPath = path.join(distFolder, 'latest.yml');
						var yamlContent = yaml.safeLoad(fs.readFileSync(yamlPath, 'utf8'));
						yamlContent.path = ['artifact', process.platform, arch, packageJson.version].join('/');
						fs.writeFileSync(yamlPath, yaml.safeDump(yamlContent));

						const versionFolder = path.join(distFolder, packageJson.version);
						const pathToDelete = path.join(distFolder, 'win-unpacked');
						fs.remove(pathToDelete)
							.then(() => {
								if (shouldMoveFileToVersionFolder) {
									move(path.join(distFolder, '*'), versionFolder).then(() => {
										process.exit();
									});
								} else {
									process.exit();
								}
							});
					}
					break;
				case 'linux':
					{
						const distFolder = path.join(__dirname, '..', 'dist');

						const yamlPath = path.join(distFolder, 'latest.yml');
						var yamlContent = yaml.safeLoad(fs.readFileSync(yamlPath, 'utf8'));
						yamlContent.path = ['artifact', process.platform, arch, packageJson.version].join('/');
						fs.writeFileSync(yamlPath, yaml.safeDump(yamlContent));

						const versionFolder = path.join(distFolder, packageJson.version);
						const pathToDelete = path.join(distFolder, 'linux-unpacked');
						fs.remove(pathToDelete)
							.then(() => {
								if (shouldMoveFileToVersionFolder) {
									move(path.join(distFolder, '*'), versionFolder).then(() => {
										process.exit();
									});
								} else {
									process.exit();
								}
							});
					}
					break;
				default:
					process.exit();
			}

		}).catch(function(error) {
			console.log('Error while building for ' + process.arch, error);
			process.exit(1);
		});
	}
};

gulp.task('build', function() {
	return gulp.src('').pipe(through.obj((file, encoding, callback) => {
		build();
		callback();
	})).pipe(gulp.dest(''));
});
