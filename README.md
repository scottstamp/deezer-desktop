# deezer-desktop-electron

**Official Deezer Electron App**

This is a proof of concept of an Electron application wrapping the Deezer website.
It uses the deezer Native SDK to provide offline capabilities and FLAC playback (not yet implemented ;) )