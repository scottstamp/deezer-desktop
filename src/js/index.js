import process from 'process';
import {app} from 'electron';
import log from 'electron-log';
import clonedeepwith from 'lodash.clonedeepwith';

log.transports.console.level = process.env.DZ_DEVTOOLS === 'yes' ? 'silly' : 'warn';
log.transports.file.level = process.env.DZ_DEVTOOLS === 'yes' ? 'silly' : 'warn';
log.transports.logS.level = false;

import App from './main/App';
import Raven from 'raven';
import Sentry from './main/Sentry';

const sentryClient = Raven
	.config('https://abd0bbee36f24c6b953e9e83af19c620:f43143178f0e40e0b62b92de67b7fa73@sentry.io/251420', {
		release: app.getVersion(),
		allowSecretKey: true,
		captureUnhandledRejections: true,
		debug: true,
		shouldSendCallback: () => {
			return Sentry.shouldSend;
		},
		dataCallback: data => {
			return clonedeepwith(data, value => {
				if (typeof value === 'string') {
					if (value.indexOf('app/assets/_statics') !== -1) {
						return `app://${value.substr(value.indexOf('app/assets/_statics'))}`;
					}
					if (value.indexOf('node_modules') !== -1) {
						return `app://${value.substr(value.indexOf('node_modules'))}`;
					}
					if (value.indexOf('app/js') !== -1) {
						return `app://${value.substr(value.indexOf('app/js'))}`;
					}
					if (value.indexOf('app/html') !== -1) {
						return `app://${value.substr(value.indexOf('app/html'))}`;
					}
				}
			});
		}
	})
	.install();

Sentry.setClient(sentryClient);

log.transports.sentry = function (msg) {
	if (msg.level === 'error') {
		Raven.captureMessage(Array.isArray(msg.data) ? msg.data.join(', ') : msg.data);
	}
};

process.on('uncaughtException', function (err) {
	Raven.captureException(err);
});

export default App;
