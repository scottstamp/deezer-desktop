import {dialog} from 'electron';
import Utils from '../../../main/Utils';
import IpcInterface, {EMITTER_PROCESS_MAIN} from '../';
import DzPlayerInterface from '../DzPlayerInterface';

class NativePlayerInterface extends IpcInterface {

	constructor() {
		super(NativePlayerInterface.ranInMain, NativePlayerInterface.ranInRenderer, NativePlayerInterface.emitterProcess);
		if (Utils.isMain()) {
			this.deezerNativeWrapper = require('../../../main/Player').default;
			this.deezerNativeWrapper.setEventCallback((event) => {
				this.emit('PlayerEvent', event);
			});
		} else {
			this.deezerNativeWrapper = false;
		}
	}

	playUrl(url) {
		this.deezerNativeWrapper.callFunction('loadUrl', [url]);
		this.deezerNativeWrapper.callFunction('play', []);
		return Promise.resolve('playUrl');
	}

	loadUrl(url) {
		this.deezerNativeWrapper.callFunction('loadUrl', [url]);
		return Promise.resolve('loadUrl');
	}

	play() {
		this.deezerNativeWrapper.callFunction('play', []);
		return Promise.resolve('play');
	}

	pause() {
		this.deezerNativeWrapper.callFunction('pause', []);
		return Promise.resolve('pause');
	}

	resume() {
		this.deezerNativeWrapper.callFunction('resume', []);
		return Promise.resolve('resume');
	}

	setVolume(volume) {
		this.deezerNativeWrapper.callFunction('setVolume', [volume]);
		return Promise.resolve();
	}

	mute() {
		this.deezerNativeWrapper.callFunction('mute', []);
		return Promise.resolve();
	}

	unMute() {
		this.deezerNativeWrapper.callFunction('unMute', []);
		return Promise.resolve();
	}

	stop() {
		this.deezerNativeWrapper.callFunction('stop', []);
		return Promise.resolve();
	}

	seek(time) {
		this.deezerNativeWrapper.callFunction('seek', [time]);
		return Promise.resolve();
	}

	getCacheSize() {
		return new Promise((resolve) => {
			resolve(this.deezerNativeWrapper.callFunction('getSmartCacheCurrentSize', []));
		});
	}

	flushCache() {
		return this.deezerNativeWrapper.clearProfilePathData();
	}

	getCacheQuota() {
		return new Promise((resolve) => {
			resolve(this.deezerNativeWrapper.getCacheQuota());
		});
	}

	setCacheQuota(quotaInKB) {
		return new Promise((resolve, reject) => {
			const result = this.deezerNativeWrapper
				.changeUserCacheQuota(quotaInKB);

			if (result) {
				resolve(result);
			} else {
				reject(result);
			}
		});
	}

	getCachePath() {
		return new Promise((resolve) => {
			resolve(this.deezerNativeWrapper.getCachePath());
		});
	}

	setCachePath() {
		return DzPlayerInterface.stop().then(() => {
			return new Promise((resolve, reject) => {
				const paths = dialog.showOpenDialog(null, {
					defaultPath: this.deezerNativeWrapper.getCachePath(),
					properties: ['openDirectory', 'createDirectory']
				});
				this.deezerNativeWrapper.changeUserProfilePath(paths[0]).then(resolve, reject);
			}).then(() => {
				return this.deezerNativeWrapper.initPlayer();
			});
		});
	}

	triggerDownload(path, checksum, tracks) {
		this.deezerNativeWrapper.callFunction('syncAddContent', [path, checksum, tracks]);
		return Promise.resolve();
	}

	setCrossFadeDuration(duration) {
		return new Promise((resolve) => {
			resolve(this.deezerNativeWrapper.callFunction('setCrossFadeDuration', [duration]));
		});
	}

	crash() {
		this.deezerNativeWrapper.crash();
		return Promise.resolve();
	}

	error() {
		return new Promise(() => {
			throw new Error('RANDOM ERROR');
		});
	}

	launchPlayerProcess() {
		this.deezerNativeWrapper.launchPlayerProcess();
		return Promise.resolve();
	}
}

NativePlayerInterface.ranInMain = [
	'playUrl',
	'loadUrl',
	'play',
	'pause',
	'resume',
	'setVolume',
	'mute',
	'unMute',
	'stop',
	'seek',
	'getCacheSize',
	'getCacheQuota',
	'getCachePath',
	'setCacheQuota',
	'setCachePath',
	'flushCache',
	'triggerDownload',
	'setCrossFadeDuration',
	'crash',
	'error',
	'launchPlayerProcess'
];
NativePlayerInterface.ranInRenderer = [];
NativePlayerInterface.emitterProcess = EMITTER_PROCESS_MAIN;

export default new NativePlayerInterface();
