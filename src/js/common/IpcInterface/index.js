import Utils from '../../main/Utils';
import log from 'electron-log';
import EventEmitter2 from 'eventemitter2';
import uuid from 'uuid/v4';

export const EMITTER_PROCESS_MAIN = 'main';
export const EMITTER_PROCESS_RENDERER = 'renderer';

export default class IpcInterface extends EventEmitter2{

	constructor(ranInMain, ranInRenderer, emitterProcess) {
		super({wildcard: true});
		this.emitterProcess = emitterProcess = emitterProcess === EMITTER_PROCESS_MAIN ? EMITTER_PROCESS_MAIN : EMITTER_PROCESS_RENDERER;
		this.setupEvents = [];

		const className = this.constructor.name;

		this.setupSender();
		ranInMain.forEach((methodName) => {
			this.setMethod(methodName, true);
		});
		ranInRenderer.forEach((methodName) => {
			this.setMethod(methodName, false);
		});

		const isMain = Utils.isMain();
		const currentProcess = isMain ? EMITTER_PROCESS_MAIN : EMITTER_PROCESS_RENDERER;
		const remoteProcess = !isMain ? EMITTER_PROCESS_MAIN : EMITTER_PROCESS_RENDERER;

		const ipcInterface = this;
		this.on('*', function(eventData) {
			const event = this.event;
			if (currentProcess === emitterProcess) {
				ipcInterface.senderAvailable.then((sender) => {
					const hasEventName = eventData && eventData.eventName;
					const shouldLog = !hasEventName || (eventData.eventName !== 'Dz.Event.Player.Progress' && eventData.eventName !== 'Dz.Event.Player.LoadingProgress');
					if (shouldLog) {
						log.debug(`Emit ${event} ${JSON.stringify(eventData)} from ${Utils.isMain() ? 'main' : 'renderer'}`);
					}
					sender.send(`${className}_Events_Emit_${currentProcess}`, event, eventData);
				});
			}
		});

		if (currentProcess !== emitterProcess) {
			this.ipcBus.on(`${className}_Events_Emit_${remoteProcess}`, (ipcEvent, eventName, eventData) => {
				const hasEventName = eventData && eventData.eventName;
				const shouldLog = !hasEventName || (eventData.eventName !== 'Dz.Event.Player.Progress' && eventData.eventName !== 'Dz.Event.Player.LoadingProgress');
				if (shouldLog) {
					log.debug(`Re-Emit ${eventName} ${JSON.stringify(eventData)} in ${Utils.isMain() ? 'main' : 'renderer'}`);
				}
				this.emit(eventName, eventData);
			});
		}
	}

	setupSender() {
		const className = this.constructor.name;
		if (Utils.isMain()) {
			this.ipcBus = require('electron').ipcMain;
			this.senderAvailable = new Promise((resolve) => {
				this.ipcBus.once(`Init_IpcInterface_${className}`, (event) => {
					resolve(event.sender);
				});
			});
		} else {
			this.ipcBus = require('electron').ipcRenderer;
			this.ipcBus.send(`Init_IpcInterface_${className}`);
			this.senderAvailable = Promise.resolve(this.ipcBus);
		}
	}

	setMethod (methodName, ranInMain) {
		const className = this.constructor.name;
		const methodCallName = `${className}_${methodName}`;
		const methodCallbackName = `${methodCallName}_callback`;

		if ((Utils.isMain() && ranInMain) || (!Utils.isMain() && !ranInMain)) {
			this.ipcBus.on(methodCallName, (event, idAndArgs) => {
				log.debug(`Executing ${className}.${methodName}(${idAndArgs.args}) in ${Utils.isMain() ? 'main' : 'renderer'}`);
				var res = this[methodName].apply(this, idAndArgs.args);
				if (typeof res !== 'undefined') {
					res
						.then((data) => {
							this.senderAvailable.then((datSend) => {
								datSend.send(`${methodCallbackName}_${idAndArgs.methodCallId}`, {data, error: false, methodCallId: idAndArgs.methodCallId});
							});
						})
						.catch((err) => {
							this.senderAvailable.then((datSend) => {
								datSend.send(`${methodCallbackName}_${idAndArgs.methodCallId}`, {err, error: true, methodCallId: idAndArgs.methodCallId});
							});
						});
				} else {
					throw new Error('All IpcInterface methods must return a promise');
				}
			});
		} else {
			this[methodName] = function() {
				const methodCallId = uuid();
				const args = [...arguments];

				log.debug(`Calling ${className}.${methodName}(${args}) from ${Utils.isMain() ? 'main' : 'renderer'}`);

				this.senderAvailable.then((datSend) => {
					datSend.send(methodCallName, {methodCallId, args});
				});

				return new Promise((resolve, reject) => {
					this.ipcBus.once(`${methodCallbackName}_${methodCallId}`, (event, arg) => {
						if (arg.error === false) {
							resolve(arg.data);
						} else {
							const usefulArgs = arg;
							delete usefulArgs.methodCallId;
							reject(`${className}.${methodName}(${args}) - ${JSON.stringify(usefulArgs)}`);
						}
					});
				});
			}.bind(this);
		}
	}
}
