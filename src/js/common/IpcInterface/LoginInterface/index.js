import {session} from 'electron';
import IpcInterface, {EMITTER_PROCESS_RENDERER} from '../';
import log from 'electron-log';
import AppInterface from '../AppInterface';

let instance = null;

function removeCookie(cookie) {
	var url = 'http' + (cookie.secure ? 's' : '') + '://' + cookie.domain + cookie.path;
	session.defaultSession.cookies.remove(url, cookie.name, error => {
		if (error)
			throw error;
		log.debug('cookie delete : ', cookie);
	});
}

class LoginInterface extends IpcInterface {

	constructor() {
		if (!instance) {
			super(LoginInterface.ranInMain, LoginInterface.ranInRenderer, LoginInterface.emitterProcess);
		}
		return instance;
	}

	triggerLogout() {
		return window.electronRenderer.deleteLoginInfo();
	}

	login() {
		log.info('Trying to log in');
		const app = require('../../../');
		this.emit('user-is-logged-out');
		return new Promise(resolve => {
			session.defaultSession.clearCache(() => {
				resolve();
				setTimeout(() => {
					app.default.loadPage();
				});
			});
		});
	}

	logout(reloadAfterLogout = true) {
		session.defaultSession.cookies.get({}, (error, cookies) => {
			if (error)
				throw error;

			AppInterface.getWebsiteDomain().then(domain => {
				for (var i = cookies.length - 1; i >= 0; i--) {
					if (cookies[i].name === 'dzr_uniq_id') {
						session.defaultSession.cookies.remove(`https://${domain}`, cookies[i].name, () => {});
						session.defaultSession.cookies.remove(`https://${domain}`, 'arl', () => {});
						session.defaultSession.cookies.remove(`https://${domain}`, 'sid', () => {
							const app = require('../../../');
							if (reloadAfterLogout) {
								app.default.loadPage();
							}
						});
					}
				}
			});
		});
		return Promise.resolve();
	}

	logoutFacebook() {
		session.defaultSession.cookies.get({}, (error, cookies) => {
			if (error)
				throw error;
			for (var i = cookies.length - 1; i >= 0; i--) {
				if (cookies[i].domain.indexOf('facebook.com') !== -1) {
					removeCookie(cookies[i]);
				}
			}
		});
		return Promise.resolve();
	}

	fbLogin(accesss_token) {
		window.electronRenderer.onFacebookLoginOk(accesss_token);
		return Promise.resolve();
	}

	switchProfile() {
		window.electronRenderer.store.dispatch({type: 'modal/SHOW_MODAL', name: 'MULTI_ACCOUNT', data: {}});
		return Promise.resolve();
	}

	isLoggedIn() {
		return new Promise(resolve => {
			try {
				const uid = this._getStoreValue('user.USER.USER_ID') || 0;
				resolve(uid && uid > 0);
			} catch (e) {
				resolve(false);
			}
		});
	}

	isFamily() {
		return new Promise(resolve => {
			try {
				const multiAccount = this._getStoreValue('user.USER.MULTI_ACCOUNT');
				resolve(multiAccount && ((multiAccount.ENABLED || multiAccount.CHILD_COUNT > 0)));
			} catch (e) {
				resolve(false);
			}
		});
	}

	isEmployee() {
		return new Promise(resolve => {
			try {
				const isEmployee = this._getStoreValue('user.USER.SETTING.global.is_employee');
				resolve(Boolean(isEmployee));
			} catch (e) {
				resolve(false);
			}
		});
	}

	getUserId() {
		return new Promise(resolve => {
			try {
				const userId = this._getStoreValue('user.USER.USER_ID');
				resolve(Number.isInteger(userId) ? userId : 0);
			} catch (e) {
				resolve(0);
			}
		});
	}

	getOfferId() {
		return new Promise(resolve => {
			try {
				const offerId = this._getStoreValue('user.OFFER_ID');
				resolve(Number.isInteger(offerId) ? offerId : 0);
			} catch (e) {
				resolve(0);
			}
		});
	}

	getCountry() {
		return new Promise(resolve => {
			try {
				const country = this._getStoreValue('user.COUNTRY');
				resolve(country);
			} catch (e) {
				resolve(0);
			}
		});
	}

	getCookie(cookieName) {
		return new Promise((resolve, reject) => {
			session.defaultSession.cookies.get({}, (error, cookies) => {
				if (error)
					throw error;
				let found = false;
				for (var i = cookies.length - 1; i >= 0; i--) {
					if (cookies[i].name === cookieName) {
						found = true;
						resolve(cookies[i].value);
					}
				}
				if (!found) {
					reject(new Error(`Could not fetch cookie ${cookieName}`));
				}
			});
		});
	}

	gatekeepIsAllowed(gatekeep) {
		return new Promise(resolve => {
			try {
				const gatekeepValue = this._getStoreValue(`user.__DZR_GATEKEEPS__.${gatekeep}`);
				resolve(gatekeepValue === true);
			} catch (e) {
				resolve(false);
			}
		});
	}

	shouldUseNativePlayer() {
		return new Promise(resolve => {
			try {
				Promise.all([
					this.gatekeepIsAllowed('electron_html5_playback'),
					this._getStoreValue('user.hasLosslessAudioRight')
				]).then(results => {
					resolve(!results[1] || results[0]);
				}).catch(() => {
					resolve(true);
				});
			} catch (e) {
				resolve(e);
			}
		});
	}

	_getStoreValue(path) {
		if (!window.electronRenderer || !window.electronRenderer.store) {
			throw new Error('store not available');
		}
		const state = window.electronRenderer.store.getState();
		if (!state) {
			throw new Error('State not readable');
		}

		path = path.split('.');
		const reducer = (o, i) => o !== null && typeof o !== 'undefined'
			? o[i]
			: o;
		try {
			return path.reduce(reducer, state);
		} catch (e) {
			throw new Error(`Path ${path} not found`);
		}
	}
}

LoginInterface.ranInMain = ['login', 'logout', 'logoutFacebook', 'getCookie'];
LoginInterface.ranInRenderer = [
	'triggerLogout',
	'gatekeepIsAllowed',
	'fbLogin',
	'switchProfile',
	'isLoggedIn',
	'isFamily',
	'isEmployee',
	'getUserId',
	'getOfferId',
	'getCountry',
	'shouldUseNativePlayer'
];
LoginInterface.emitterProcess = EMITTER_PROCESS_RENDERER;

export default new LoginInterface();
