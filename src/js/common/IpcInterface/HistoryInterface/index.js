import IpcInterface, {EMITTER_PROCESS_MAIN} from '../';
import Utils from '../../../main/Utils';

let instance = null;

class HistoryInterface extends IpcInterface {

	constructor() {
		if (!instance) {
			super(HistoryInterface.ranInMain, HistoryInterface.ranInRenderer, HistoryInterface.emitterProcess);
			instance = this;
			this.window = false;
		}
		return instance;
	}

	canGoBack() {
		if (!this.window) {
			return false;
		}
		return Promise.resolve(this.window.webContents.canGoBack());
	}

	goBack() {
		if (!this.window) {
			return false;
		}
		return Promise.resolve(this.window.webContents.goBack());
	}

	goTo(path) {
		window.reactRouter.push(path);
	}

	canGoForward() {
		if (!this.window) {
			return false;
		}
		return Promise.resolve(this.window.webContents.canGoForward());
	}

	goForward() {
		if (!this.window) {
			return false;
		}
		return Promise.resolve(this.window.webContents.goForward());
	}

	closeWindow() {
		if (!this.window) {
			return false;
		}

		if (Utils.isMac()) {
			// Prevent issue when closing window in full screen on OSX
			if (this.window.isFullScreen()) {
				this.window.setFullScreen(false);
			}
			return Promise.resolve(this.window.hide());
		}

		return Promise.resolve(this.window.close());
	}

	resetHistory() {
		if (!this.window) {
			return false;
		}
		this.window.webContents.clearHistory();
		return Promise.resolve();
	}

	_setWindow(window) {
		this.window = window;
	}
}

HistoryInterface.ranInMain = ['canGoBack', 'goBack', 'canGoForward', 'goForward', 'closeWindow', 'resetHistory'];
HistoryInterface.ranInRenderer = ['goTo'];
HistoryInterface.emitterProcess = EMITTER_PROCESS_MAIN;

export default new HistoryInterface();
