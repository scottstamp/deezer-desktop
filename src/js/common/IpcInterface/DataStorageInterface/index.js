import Utils from '../../../main/Utils';
import IpcInterface, {EMITTER_PROCESS_MAIN} from '../';

class DataStorageInterface extends IpcInterface {

	constructor() {
		super(DataStorageInterface.ranInMain, DataStorageInterface.ranInRenderer, DataStorageInterface.emitterProcess);
		if (!Utils.isMain()) {
			this.db = require('localforage');
		} else {
			this.db = false;
		}
	}

	get(key) {
		return new Promise((resolve, reject) => {
			this.db.getItem(key, (err, value) => {
				if (err) {
					reject(err);
				} else {
					resolve(value);
				}
			});
		});
	}

	set(key, value) {
		return new Promise((resolve, reject) => {
			this.db.setItem(key, value, (err) => {
				if (err) {
					reject(err);
				} else {
					resolve();
				}
			});
		});
	}
}

DataStorageInterface.ranInMain = [];
DataStorageInterface.ranInRenderer = [
	'get',
	'set'
];
DataStorageInterface.emitterProcess = EMITTER_PROCESS_MAIN;

export default new DataStorageInterface();
