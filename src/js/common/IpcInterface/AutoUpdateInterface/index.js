import IpcInterface, {EMITTER_PROCESS_MAIN} from '../';

class AutoUpdateInterface extends IpcInterface {

	constructor() {
		super(AutoUpdateInterface.ranInMain, AutoUpdateInterface.ranInRenderer, AutoUpdateInterface.emitterProcess);
	}

	restartForUpdate() {
		require('electron-updater').autoUpdater.quitAndInstall();
		return Promise.resolve();
	}
}

AutoUpdateInterface.ranInMain = ['restartForUpdate'];
AutoUpdateInterface.ranInRenderer = [];
AutoUpdateInterface.emitterProcess = EMITTER_PROCESS_MAIN;

export default new AutoUpdateInterface();
