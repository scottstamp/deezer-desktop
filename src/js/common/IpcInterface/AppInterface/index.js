import IpcInterface, {EMITTER_PROCESS_MAIN} from '../';
import {session, app} from 'electron';
import {release, type} from 'os';
import settings from 'electron-settings';

class AppInterface  extends IpcInterface {

	constructor() {
		super(AppInterface.ranInMain, AppInterface.ranInRenderer, AppInterface.emitterProcess);
	}

	clearAppDataAndRestart () {
		return new Promise(resolve => {
			require('../../../main/Player').default.clearProfilePathData().then(() => {
				settings.deleteAll();
				session.defaultSession.clearCache(() => {
					session.defaultSession.clearStorageData(() => {
						setTimeout(() => {
							app.relaunch();
							app.exit();
							resolve();
						});
					});
				});
			});
		});
	}

	hasTriforceFlag () {
		return Promise.resolve(process.env.DZ_FORCE_TRIFORCE === 'yes');
	}

	hasDevEnv () {
		return Promise.resolve(process.env.DZ_DEV_ENV ? process.env.DZ_DEV_ENV : false);
	}

	getDzTld() {
		return Promise.resolve(process.env.DZ_DEV_ENV ? 'deezerdev.com' : 'deezer.com');
	}

	getWebsiteDomain() {
		return Promise.resolve(process.env.DZ_DEV_ENV ? `${process.env.DZ_DEV_ENV}-www.deezerdev.com` : 'www.deezer.com');
	}

	getOriginalUserAgent() {
		return Promise.resolve(require('../../../').default.originalUserAgent);
	}

	getSystemSentryTags() {
		return Promise.resolve({
			environment: process.env.DEEZER_DESKTOP_ENV === 'dev' ? 'development' : 'production',
			os: `${type()} ${release()}`,
			'os.name': type(),
			arch: process.arch
		});
	}
}
AppInterface.ranInMain = [
	'clearAppDataAndRestart',
	'hasTriforceFlag',
	'hasDevEnv',
	'getDzTld',
	'getWebsiteDomain',
	'getOriginalUserAgent'
];
AppInterface.ranInRenderer = [];
AppInterface.emitterProcess = EMITTER_PROCESS_MAIN;

export default new AppInterface();
