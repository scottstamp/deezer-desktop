import NativePlayerInterface from './NativePlayerInterface';
import DzPlayerInterface from './DzPlayerInterface';
import HistoryInterface from './HistoryInterface';
import LoginInterface from './LoginInterface';
import AutoUpdateInterface from './AutoUpdateInterface';
import AutoStartInterface from './AutoStartInterface';
import ConnectivityInterface from './ConnectivityInterface';
import AppInterface from './AppInterface';
import DataStorageInterface from './DataStorageInterface';

export default {
	NativePlayerInterface,
	DzPlayerInterface,
	HistoryInterface,
	LoginInterface,
	AutoUpdateInterface,
	AutoStartInterface,
	ConnectivityInterface,
	AppInterface,
	DataStorageInterface
};
