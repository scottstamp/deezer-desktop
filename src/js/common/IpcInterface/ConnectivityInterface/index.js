import IpcInterface, {EMITTER_PROCESS_MAIN} from '../';

class ConnectivityInterface extends IpcInterface {

	constructor() {
		super(ConnectivityInterface.ranInMain, ConnectivityInterface.ranInRenderer, ConnectivityInterface.emitterProcess);
		this.offline =  false;
		this.on('connectivity-available', () => {
			this.offline = false;
			this.resetChromecastList();
		});
		this.on('connectivity-not-available', () => {
			this.offline = true;
			this.resetChromecastList();
		});
		this.on('networks-list-changed', () => {
			this.resetChromecastList();
		});
	}

	// This method is not proxied, main and renderer should be up to date with the value
	isOffline() {
		return this.offline;
	}

	resetChromecastList() {
		if (window && window.chrome && window.chrome.cast && typeof window.chrome.cast.startBrowsing === 'function'){
			window.chrome.cast.startBrowsing();
		}
		return Promise.resolve();
	}
}

ConnectivityInterface.ranInMain = [];
ConnectivityInterface.ranInRenderer = [
	'resetChromecastList'
];
ConnectivityInterface.emitterProcess = EMITTER_PROCESS_MAIN;

export default new ConnectivityInterface();
