import Utils from '../../../main/Utils';
import log from 'electron-log';
import IpcInterface, {EMITTER_PROCESS_RENDERER} from '../';

let instance = null;

class DzPlayerInterface extends IpcInterface {

	constructor() {
		if (!instance) {
			super(DzPlayerInterface.ranInMain, DzPlayerInterface.ranInRenderer, DzPlayerInterface.emitterProcess);
			instance = this;
		}
		return instance;
	}

	next() {
		return this._setPlayer().then(resolve => {
			this.player.control.nextSong();
			resolve();
		}).catch(error => log.info(error.message));
	}

	prev() {
		return this._setPlayer().then(resolve => {
			this.player.control.prevSong();
			resolve();
		}).catch(error => log.info(error.message));
	}

	togglePause() {
		return this._setPlayer().then(resolve => {
			this.player.control.togglePause();
			resolve();
		}).catch(error => log.info(error.message));
	}

	stop() {
		return new Promise((resolve) => {
			this._setPlayer().then(() => {
				this.player.control.stop();
				resolve();
			});
		}).catch(error => log.info(error.message));
	}

	increaseVolume(percentage) {
		return this._setPlayer().then(resolve => {
			percentage = percentage || 0.10;
			this.player.control.setVolume(Math.min(1, this.player.volume + percentage));
			resolve();
		}).catch(error => log.info(error.message));
	}

	decreaseVolume(percentage) {
		return this._setPlayer().then(resolve => {
			percentage = percentage || 0.10;
			this.player.control.setVolume(Math.min(1, this.player.volume - percentage));
			resolve();
		}).catch(error => log.info(error.message));
	}

	enableShuffle() {
		return this._setPlayer().then(resolve => {
			this.player.control.setShuffle(true);
			resolve();
		}).catch(error => log.info(error.message));
	}

	disableShuffle() {
		return this._setPlayer().then(resolve => {
			this.player.control.setShuffle(false);
			resolve();
		}).catch(error => log.info(error.message));
	}

	setRepeatMode(mode = 0) {
		return this._setPlayer().then(resolve => {
			this.player.control.setRepeat(mode);
			resolve();
		}).catch(error => log.info(error.message));
	}

	isPlaying() {
		return this._setPlayer().then(resolve => {
			resolve(this.player.playing);
		}).catch(error => log.info(error.message));
	}

	_setPlayer() {
		return new Promise((resolve, reject) => {
			if (!Utils.isMain()) {
				this.player = window.dzPlayer;

				if (window.dzPlayer) {
					resolve();
				} else {
					reject(new Error('Could not access dzPlayer in renderer'));
				}
			} else {
				reject(new Error('Tried to access dzPlayer from main'));
			}
		});
	}
}

DzPlayerInterface.ranInMain = [];
DzPlayerInterface.ranInRenderer = [
	'next',
	'prev',
	'togglePause',
	'stop',
	'increaseVolume',
	'decreaseVolume',
	'enableShuffle',
	'disableShuffle',
	'setRepeatMode',
	'isPlaying'
];
DzPlayerInterface.emitterProcess = EMITTER_PROCESS_RENDERER;

export default new DzPlayerInterface();
