import IpcInterface, {EMITTER_PROCESS_MAIN} from '../';
import Utils from '../../../main/Utils';
import {app} from 'electron';
import log from 'electron-log';
import settings from 'electron-settings';

const LOGIN_HAS_BEEN_INIT_KEY = 'loginHasBeenInit';

class AutoStartInterface  extends IpcInterface {

	constructor() {
		super(AutoStartInterface.ranInMain, AutoStartInterface.ranInRenderer, AutoStartInterface.emitterProcess);
	}

	initLoginItem () {
		if (!settings.has(LOGIN_HAS_BEEN_INIT_KEY)) {
			this.enableAutoStart();
			settings.set(LOGIN_HAS_BEEN_INIT_KEY, true);
		}
	}

	isOpenAtLogin() {
		const {openAtLogin} = app.getLoginItemSettings();
		log.debug(`Will Open at Login ${openAtLogin}`);
		return Promise.resolve(openAtLogin || false);
	}

	isOpenAsHidden() {
		const {openAsHidden} = app.getLoginItemSettings();
		log.debug(`Will Open as Hidden ${openAsHidden}`);
		return Promise.resolve(openAsHidden || false);
	}

	wasOpenAtLogin() {
		const {wasOpenAtLogin} = app.getLoginItemSettings();
		log.debug(`Was Open at Login ${wasOpenAtLogin}`);
		return Promise.resolve(wasOpenAtLogin || false);
	}

	wasOpenAsHidden() {
		const {wasOpenAsHidden} = app.getLoginItemSettings();
		log.debug(`Was Open as Hidden ${wasOpenAsHidden}`);
		return Promise.resolve(wasOpenAsHidden || false);
	}

	enableAutoStart() {
		log.debug('Enabling Open At Login');
		this._updateLoginItem({openAtLogin: true});
		settings.set(LOGIN_HAS_BEEN_INIT_KEY, true);
		return Promise.resolve();
	}

	disableAutoStart() {
		log.debug('Disabling Open At Login');

		if (Utils.isMac() && Utils.getPlatformVersion().indexOf('10.13') === 0) {
			const appName = Utils.isProd() ? app.getName() : 'Electron';
			require('child_process').exec(`osascript -e 'tell application "System Events" to delete login item "${appName}"'`);
			return new Promise(resolve => {
				setTimeout(resolve, 200);
			});
		} else {
			this._updateLoginItem({openAtLogin: false});
		}
		settings.set(LOGIN_HAS_BEEN_INIT_KEY, true);
		return Promise.resolve();
	}

	enableOpenAsHidden() {
		log.debug('Enabling Open as Hidden');
		this._updateLoginItem({openAsHidden: true});
		settings.set(LOGIN_HAS_BEEN_INIT_KEY, true);
		return Promise.resolve();
	}

	disableOpenAsHidden() {
		log.debug('Disabling Open as Hidden');
		this._updateLoginItem({openAsHidden: false});
		settings.set(LOGIN_HAS_BEEN_INIT_KEY, true);
		return Promise.resolve();
	}

	_updateLoginItem (update) {
		app.setLoginItemSettings(
			Object.assign({}, app.getLoginItemSettings(), update)
		);
		return Promise.resolve();
	}
}
AutoStartInterface.ranInMain = [
	'isOpenAtLogin',
	'isOpenAsHidden',
	'wasOpenAtLogin',
	'wasOpenAsHidden',
	'enableAutoStart',
	'disableAutoStart',
	'enableOpenAsHidden',
	'disableOpenAsHidden',
];
AutoStartInterface.ranInRenderer = [];
AutoStartInterface.emitterProcess = EMITTER_PROCESS_MAIN;

export default new AutoStartInterface();
