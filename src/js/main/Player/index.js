import ipc from 'node-ipc';
import log from 'electron-log';
import settings from 'electron-settings';
import {app} from 'electron';
import LoginInterface from '../../common/IpcInterface/LoginInterface';
import Utils from '../Utils';
import path from 'path';
import fs from 'fs-extra';
import rimraf from 'rimraf';
import childProcess from 'child_process';
import uuid from 'uuid/v4';
import ipcConfig from './ipcConfig';
Object.assign(ipc.config, ipcConfig);

const USER_PROFILE_PATH_SETTING_KEY = 'userProfilePath';
const USER_CACHE_QUOTA_SETTING_KEY = 'userCacheQuota';
const DEFAULT_PATH = path.join(app.getPath('userData'), 'sdk_cache');
const DEFAULT_QUOTA = 1 * 1024 * 1024;

class Defer {
	constructor() {
		this.resolve = () => {};
		this.reject = () => {};

		this.promise = new Promise((resolve, reject) => {
			this.resolve = resolve;
			this.reject = reject;
		});
	}
}

class Player {
	constructor() {
		this.isShuttingDown = false;
		this.isNativeProcessLoaded = false;
		this.isNativeProcessLoading = false;
		this.isLoggedIn = false;
		this.childProcess = false;
		this.eventCallback = () => {};
		this.callDefers = {};
	}

	init() {
		LoginInterface.on('user-is-logged-in', () => {
			this.isLoggedIn = true;
			LoginInterface.shouldUseNativePlayer().then(shouldUseNativePlayer => {
				if (shouldUseNativePlayer) {
					this.launchPlayerProcess().then(() => {
						return this.connectToPlayerProcess();
					}).then(() => {
						this.initPlayer();
					}).catch(() => {
						log.info('Could not start native player process or already started');
					});
				}
			});
		});
		LoginInterface.on('user-is-logged-out', () => {
			this.isLoggedIn = false;
			this.shutDownPlayer().then(() => {
				this.killPlayerProcess();
			});
		});
		process.on('exit', () => {
			this.killPlayerProcess();
		});
	}

	setEventCallback(eventCallback) {
		this.eventCallback = eventCallback;
	}

	launchPlayerProcess() {
		this.isNativeProcessLoading = true;
		return new Promise((resolve, reject) => {
			if (this.childProcess) {
				log.info('Player already started');
				reject();
				return;
			}
			this.childProcess = childProcess.fork(path.join(__dirname, 'playerThreadServer.js'));
			this.childProcess.on('exit', () => {
				this.childProcess = false;
				if (this.isShuttingDown) {
					return false;
				}
				log.error('Player process died unexpectedly, deleting cache path');
				rimraf(this.getCachePath(), () => {
					this.eventCallback({eventName: 'Dz.Event.Player.ProcessError'});
				});
			});
			resolve();
		});
	}

	killPlayerProcess() {
		this.isNativeProcessLoaded = false;
		this.isShuttingDown = true;
		if (this.childProcess) {
			this.childProcess.kill();
			this.childProcess = false;
		}
	}

	connectToPlayerProcess() {
		return new Promise(resolve => {
			ipc.connectTo('nodeDeezerNative', () => {
				ipc.of.nodeDeezerNative.on('connect', () => {
					if (this.isLoggedIn) {
						this.initPlayer();
						this.eventCallback({eventName: 'Dz.Event.Player.ProcessRestarted'});
					}
					ipc.of.nodeDeezerNative.emit('initEvents');
					ipc.of.nodeDeezerNative.on('player.event', (data) => {
						if (data && data.eventName && data.eventName === 'Dz.Event.Player.RenderTrackStartFailure') {
							this.clearProfilePathData();
						}
						this.eventCallback(data);
					});
					this.isNativeProcessLoading = false;
					this.isNativeProcessLoaded = true;
					resolve();
				});
				ipc.of.nodeDeezerNative.on('functionReturn', ({callId, result}) => {
					if (this.callDefers[callId]) {
						this.callDefers[callId].resolve(result);
						delete this.callDefers[callId];
					}
				});
			});
		});
	}

	callFunction(functionName, params) {
		if (this.isNativeProcessLoaded !== true && ['stop', 'shutDownPlayer', 'shutDownConnect'].indexOf(functionName) !== -1) {
			return;
		}
		const callId = uuid();
		const defer = new Defer();
		try{
			ipc.of.nodeDeezerNative.emit('execFunction', {callId, functionName, params});
		} catch(e) {
			log.info(`Could not call method ${functionName} on native player`);
		}
		this.callDefers[callId] = defer;
		return defer.promise;
	}

	crash() {
		ipc.of.nodeDeezerNative.emit('pleaseCrash');
	}

	getCachePath() {
		if (settings.has(USER_PROFILE_PATH_SETTING_KEY)) {
			return settings.get(USER_PROFILE_PATH_SETTING_KEY);
		}
		return path.join(DEFAULT_PATH);
	}

	getCacheQuota() {
		if (settings.has(USER_CACHE_QUOTA_SETTING_KEY)) {
			return settings.get(USER_CACHE_QUOTA_SETTING_KEY);
		}
		return DEFAULT_QUOTA;
	}

	changeUserProfilePath(newPath) {
		return this.shutDownPlayer()
			.then(() => {
				return new Promise((resolve, reject) => {
					return fs.move(this.getCachePath(), newPath, {overwrite: true}, err => {
						if (err && !Utils.isWindows()) {
							reject(err);
						} else {
							resolve();
						}
					});
				});
			})
			.then(() => {
				settings.set(USER_PROFILE_PATH_SETTING_KEY, newPath);
				return Promise.resolve();
			})
			.catch(() => {
				log.error('Could not shutdown player and change profile path');
			});
	}

	changeUserCacheQuota(quotaInKB) {
		settings.set(USER_CACHE_QUOTA_SETTING_KEY, quotaInKB);
		return this.callFunction('setSmartCacheQuota', [quotaInKB]);
	}

	clearProfilePathData() {
		log.debug('Asking for clearProfilePathData');
		return new Promise((resolve, reject) => {
			const resFlush = this.callFunction('setSmartCacheQuota', [0]);
			setTimeout(() => {
				const resReset = this.callFunction('setSmartCacheQuota', [this.getCacheQuota()]);
				if (resFlush && resReset) {
					resolve();
				} else {
					reject();
				}
			}, 1000);
		});
	}

	initPlayer() {
		return Promise.all[
			this.callFunction('initConnect', [{
				disableLog: false,
				appId: 'DEEZER_NODE_ELECTRON',
				productId: null,
				productBuildId: null,
				userProfilePath: this.getCachePath(),
				accessToken: 'NULL'
			}]),
			this.callFunction('initPlayer', [{
				reportLoadingProgress: false
			}]),
			this.callFunction('setSmartCacheQuota', [this.getCacheQuota()])
		];
	}

	shutDownPlayer() {
		this.callFunction('stop', []);
		this.callFunction('shutDownPlayer', []);
		this.callFunction('shutDownConnect', []);

		return new Promise(resolve => {
			setTimeout(resolve, 1000);
		});
	}
}

export default new Player();
