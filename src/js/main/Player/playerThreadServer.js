import DeezerNative from 'node-deezer-native';
import ipc from 'node-ipc';
import ipcConfig from './ipcConfig';
Object.assign(ipc.config, ipcConfig);

ipc.serve(() => {
	ipc.server.on('execFunction', (data, socket) => {
		const {callId, functionName, params} = data;
		try{
			const result = DeezerNative[functionName](...params);
			ipc.server.emit(socket, 'functionReturn', {callId, result});
		} catch(e) {
			ipc.log('Error handling native call', e);
		}
	});

	ipc.server.on('initEvents', (data, socket) => {
		DeezerNative.onEvent(event => {
			ipc.server.emit(socket, 'player.event', event);
		});
	});

	ipc.server.on('pleaseCrash', () => {
		process.exit(1);
	});

	ipc.server.on('error', e => {
		ipc.log('Player process encountered an error', e);
		process.exit(0);
	});

	ipc.server.on('socket.disconnected', () => {
		process.exit(0);
	});
});

const stopServer = () => {
	ipc.server.stop();
};

//do something when app is closing
process.on('exit', stopServer);

ipc.server.start();
