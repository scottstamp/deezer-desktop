import os from 'os';
const username = os.userInfo().username.toLowerCase();
export default {
	id: 'nodeDeezerNative',
	retry: 500,
	appspace: `com.deezer.desktop.app.${username}.`,
	silent: true
};
