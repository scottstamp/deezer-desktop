import chromecast_bridge from 'electron-chromecast';

chromecast_bridge(function (receivers) {
	window.__DZR_CAST_RECEIVERS__ = receivers;
	return new Promise((resolve, reject) => {
		window.__DZR_CAST_REQUEST_SESSION_PROMISE_RESOLVE__ = resolve;
		window.__DZR_CAST_REQUEST_SESSION_PROMISE_REJECT__ = reject;
	});
});
