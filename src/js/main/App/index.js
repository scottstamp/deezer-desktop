import path from 'path';
import url, {URL} from 'url';
import {app, session, BrowserWindow, shell} from 'electron';
import Utils from '../Utils';
import Player from '../Player';
import {saveLocaleToCookie, getDzLocaleFromLocale, init as initI18n} from '../I18n';
import WindowStateManager from '../WindowStateManager';
import log from 'electron-log';
import ApplicationMenu from './../ApplicationMenu';
import PowerSave from './../PowerSave';
import interfaces from '../../common/IpcInterface/interfaces';
import ConnectivityStatus from './../ConnectivityStatus';
import AutoUpdater from './../AutoUpdater';
// import UWPCom from './../UWPCom';

class App {
	constructor() {
		log.debug('Construct App');
		this.app = app;
		this.app.setName('Deezer');
		this.window = null;
		this.oauthWindow = null;
		this.windowStateManager = new WindowStateManager(this);
		this.originalUserAgent = '';

		Promise.all([interfaces.AppInterface.getDzTld(), interfaces.AppInterface.getWebsiteDomain()]).then(result => {
			this.tld = result[0];
			this.domain = result[1];
			this.init();
		});
	}

	init() {
		log.debug('Init App');

		var isSecondInstance = app.makeSingleInstance(() => {
			if (this.getWindow()) {
				if (this.getWindow().isMinimized()) {
					this.getWindow().restore();
				}
				this.getWindow().focus();
			}
		});

		if (isSecondInstance) {
			log.warn('Quitting second instance of the app');
			app.quit();
			return;
		}

		Player.init();

		app.setAppUserModelId('com.deezer.deezer-desktop');

		app.on('ready', () => {
			log.debug('App is ready');
			// Modify the user agent for all requests to the following urls.
			const dzrFilter = {
				urls: [`*.${this.tld}`]
			};
			session.defaultSession.webRequest.onBeforeSendHeaders(dzrFilter, (details, callback) => {
				if (details && details.url && details.url.indexOf(this.tld) !== -1) {
					details.requestHeaders.Origin = `https://${this.domain}`;
				}
				callback({cancel: false, requestHeaders: details.requestHeaders});
			});

			const smartAdsFilter = {
				urls: ['*.*']
			};
			session.defaultSession.webRequest.onBeforeRequest(smartAdsFilter, (details, callback) => {
				if (details.url.indexOf('blob:file') !== -1) {
					callback({cancel: false});
					return;
				}
				if (details.url.indexOf('file:') === -1) {
					callback({cancel: false});
					return;
				}
				const encodedUrl = new URL(details.url).toString();
				const encodedAppPath = new URL(`${Utils.isWindows() ? 'file:///' : 'file://'}${app.getAppPath()}`).toString();
				if (encodedUrl.indexOf(encodedAppPath) !== 0) {
					callback({
						cancel: false,
						redirectURL: details.url.replace('file://', 'https://')
					});
					return;
				}
				callback({cancel: false});
			});

			log.debug('Creating Window');
			this.createWindow();
			initI18n();
			saveLocaleToCookie().then(() => {
				this.loadWindow();
			});
			ApplicationMenu.setupStatusListeners();
			PowerSave.setupStatusListeners();
			ApplicationMenu.setMenu();
			interfaces.AutoStartInterface.initLoginItem();

			// Save deezer session cookies for two weeks
			var cookies = session.defaultSession.cookies;
			cookies.on('changed', function(event, cookie, cause, removed) {
				if (cookie.session && !removed && cookie.domain.indexOf(this.tld) !== -1) {
					log.debug('Persisting cookie', {
						name: cookie.name,
						value: cookie.value
					}, 'for two weeks');
					var cookieUrl = url.format({
						protocol: (!cookie.httpOnly && cookie.secure)
							? 'https'
							: 'http',
						host: cookie.domain,
						path: cookie.path
					});
					cookies.set({
						url: cookieUrl,
						name: cookie.name,
						value: cookie.value,
						domain: cookie.domain,
						path: cookie.path,
						secure: cookie.secure,
						httpOnly: cookie.httpOnly,
						expirationDate: Math.floor(new Date().getTime() / 1000) + 1209600
					}, function(err) {
						if (err) {
							log.warn('Error trying to persist cookie', err, cookie);
						}
					});
				}
			});
		});

		app.on('window-all-closed', () => {
			log.debug('Window All Closed');
			app.quit();
		});

		app.on('activate', () => {
			log.debug('App activated');
			if (this.getWindow() === null) {
				this.createWindow();
				this.loadWindow();
			} else {
				this.getWindow().show();
			}
		});

		app.on('before-quit', () => {
			PowerSave.disable();
			log.debug('App on before quit');
			interfaces.HistoryInterface.emit('close-app');
		});
	}

	createWindow() {
		// Create the browser window.
		const baseWindowParams = {
			title: 'Deezer Desktop',
			minWidth: 990,
			minHeight: 600,
			titleBarStyle: 'hiddenInset',
			backgroundColor: '#23232C',
			show: false,
			webPreferences: {
				devTools: Utils.hasDevTools(),
				preload: path.join(__dirname, 'chromecast.js')
			}
		};

		// Set window icon, needed for win10
		if (Utils.isWindows()) {
			baseWindowParams.icon = path.join(__dirname, '..', '..', '..', '..', '..', 'build/win/app.ico');
		}

		const currentWindowState = this.windowStateManager.getState();

		this.window = new BrowserWindow(Object.assign({}, baseWindowParams, currentWindowState));

		this.originalUserAgent = this.getWebContents().getUserAgent();

		this.getWebContents().setUserAgent(`Deezer/${this.app.getVersion()} (Electron; ${Utils.getLogFormattedOsName()}/${Utils.getPlatformVersion()}; Desktop; ${getDzLocaleFromLocale(this.app.getLocale())})`);

		if (Utils.isMac()) {
			this.getWindow().on('moved', () => {
				this.windowStateManager.saveState();
			});
		} else {
			this.getWindow().on('move', () => {
				this.windowStateManager.saveState();
			});
		}
		this.getWindow().on('resize', () => {
			this.windowStateManager.saveState();
		});
		this.getWindow().on('maximize', () => {
			this.windowStateManager.saveState();
		});
		this.getWindow().on('unmaximize', () => {
			this.windowStateManager.saveState();
		});

		this.getWindow().once('ready-to-show', () => {
			if (currentWindowState.maximized) {
				this.getWindow().maximize();
			} else {
				if (Utils.isWindows()) {
					const curWinSize = this.getWindow().getSize();
					const curContentSize = this.getWindow().getContentSize();
					const minWinSize = this.getWindow().getMinimumSize();
					const finalMinSize = [
						minWinSize[0] + curWinSize[0] - curContentSize[0],
						minWinSize[1] + curWinSize[1] - curContentSize[1]
					];

					this.getWindow().setMinimumSize(finalMinSize[0], finalMinSize[1]);

					if (curWinSize[0] < finalMinSize[0]) {
						this.getWindow().setSize(finalMinSize[0], curWinSize[1]);
					}

					if (curWinSize[1] < finalMinSize[1]) {
						this.getWindow().setSize(curWinSize[0], finalMinSize[1]);
					}
					log.debug('Setting minimum size to ', finalMinSize[0], finalMinSize[1]);
				}
			}
			this.getWindow().show();
		});

		if (!Utils.isProd() && Utils.hasDevTools()) {
			this.getWebContents().openDevTools();
		}

		if (Utils.isElectronConnect()) {
			log.debug('Loading electron-connect');
			require('electron-connect').client.create();
		}
	}

	loadWindow() {

		if (this.oauthWindow) {
			this.oauthWindow.close();
		}

		interfaces.HistoryInterface._setWindow(this.getWindow());
		const report = () => {
			const canGoBackPromise = interfaces.HistoryInterface.canGoBack();
			const canGoForwardPromise = interfaces.HistoryInterface.canGoForward();

			Promise.all([canGoBackPromise, canGoForwardPromise]).then((result) => {
				interfaces.HistoryInterface.emit('update-data', {
					canGoBack: result[0],
					canGoForward: result[1]
				});
			});
		};
		this.getWebContents().on('did-navigate-in-page', report);
		this.getWebContents().on('did-navigate', report);

		if (Utils.isWindows()) {
			this.getWindow().on('app-command', (e, cmd) => {
				if (cmd === 'browser-backward') {
					interfaces.HistoryInterface.canGoBack().then((canGoBack) => {
						if (canGoBack) {
							interfaces.HistoryInterface.goBack();
						}
					});
				}
				if (cmd === 'browser-forward') {
					interfaces.HistoryInterface.canGoForward().then((canGoForward) => {
						if (canGoForward) {
							interfaces.HistoryInterface.goForward();
						}
					});
				}
			});
		}

		this.loadPage();

		const handleRedirect = (event, targetUrl) => {
			log.debug(`Asking opening of ${targetUrl}`);

			if (targetUrl.indexOf('dialog/oauth') !== -1) {
				event.preventDefault();

				if (this.oauthWindow) {
					this.oauthWindow.removeAllListeners();
					this.oauthWindow.destroy();
				}
				this.oauthWindow = new BrowserWindow({
					parent: this.getWindow(), show: false,
					// titleBarStyle: 'customButtonsOnHover',
					webPreferences: {
						nodeIntegration: false
					}
				});

				const openFbLoginWindow = () => {
					this.oauthWindow.loadURL('https://www.facebook.com/dialog/oauth?client_id=241284008322&redirect_uri=https://www.facebook.com/connect/login_success.html&scope=email,user_friends,public_profile&response_type=token&display=popup');
					this.oauthWindow.webContents.on('did-get-redirect-request', (redirectEvent, oldUrl, newUrl) => {
						var raw_code = /access_token=([^&]*)/.exec(newUrl) || null;
						var access_token = (raw_code && raw_code.length > 1)
							? raw_code[1]
							: null;
						// var error = /\?error=(.+)$/.exec(newUrl);
						if (access_token) {
							this.oauthWindow.close();
							interfaces.LoginInterface.fbLogin(access_token);
						}
					});
					this.oauthWindow.once('ready-to-show', () => {
						this.oauthWindow.show();
					});
				};

				if (event.sender.getURL().indexOf('#/login') !== -1) {
					interfaces.LoginInterface.logoutFacebook().then(() => {
						openFbLoginWindow();
					});
				} else {
					openFbLoginWindow();
				}
			} else if (null !== this.getWindow() && targetUrl !== this.getWindow().getURL()) {
				const currentUrl = url.parse(this.getWindow().getURL());
				const basePageUrl = url.format(currentUrl).replace(currentUrl.hash, '');
				if (targetUrl.indexOf(basePageUrl) === 0) {
					targetUrl = targetUrl.replace(`${basePageUrl}#/`, '');
				} else if (targetUrl.indexOf('file://') === 0) {
					targetUrl = targetUrl.replace(Utils.isWindows() ? 'file:///' : 'file://', '');
					targetUrl = path.relative('/', targetUrl);
				}

				if (targetUrl.indexOf('http') !== 0) {
					targetUrl = `https://${this.domain}/` + targetUrl;
				}
				log.debug(`Opening ${targetUrl} externally`);
				event.preventDefault();
				shell.openExternal(targetUrl);
			}
		};

		this.getWindow().webContents.on('new-window', handleRedirect);

		this.getWebContents().on('crashed', () => {
			log.warn('WebContents crashed');
			this.getWindow().close();
			this.getApp().quit();
		});

		this.getWindow().on('unresponsive', () => {
			log.warn('Window is not responsive');
		});
	}

	loadPage() {
		this.getWebContents().clearHistory();
		this.getWindow().loadURL(url.format({
			pathname: path.join(__dirname, '../../../html/index.html'),
			protocol: 'file:',
			slashes: true,
			hash: '/'
		}));
	}

	getApp() {
		return this.app;
	}

	getWindow() {
		return this.window;
	}

	getWebContents() {
		return this.window.webContents;
	}
}

export default new App();
