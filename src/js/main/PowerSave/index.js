import settings from 'electron-settings';
import {powerSaveBlocker} from 'electron';
import DzPlayerInterface from '../../common/IpcInterface/DzPlayerInterface';
import log from 'electron-log';

const POWER_SAVE_ID_SETTING_KEY = 'POWER_SAVE_ID';

class PowerSave {

	constructor() {
		this.isPlaying = false;
	}

	setupStatusListeners() {
		if (settings.has(POWER_SAVE_ID_SETTING_KEY)) {
			log.error('Power save was still enabled after last app open, disabling');
			this.disable();
		}
		DzPlayerInterface.on('playing-changed', value => {
			if (value && !this.isPlaying) {
				this.enable();
			}
			if (!value && this.isPlaying) {
				this.disable();
			}
			this.isPlaying = value;
		});
	}

	enable() {
		log.info('Enabling power save block');
		const id = powerSaveBlocker.start('prevent-app-suspension');
		settings.set(POWER_SAVE_ID_SETTING_KEY, id);
	}

	disable() {
		log.info('Disabling power save block');
		const id = settings.get(POWER_SAVE_ID_SETTING_KEY);
		if (id && powerSaveBlocker.isStarted(id)) {
			powerSaveBlocker.stop(id);
		}
		settings.delete(POWER_SAVE_ID_SETTING_KEY);
	}
}

export default new PowerSave();
