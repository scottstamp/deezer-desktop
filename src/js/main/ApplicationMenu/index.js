import {app, Menu, globalShortcut} from 'electron';
import Utils from '../Utils';
import {t} from '../I18n';
import DzPlayerInterface from '../../common/IpcInterface/DzPlayerInterface';
import LoginInterface from '../../common/IpcInterface/LoginInterface';
import HistoryInterface from '../../common/IpcInterface/HistoryInterface';
import AppInterface from '../../common/IpcInterface/AppInterface';
import log from 'electron-log';
import debounce from 'lodash.debounce';


function triggerIfLogged(action) {
	return () => {
		LoginInterface
			.isLoggedIn()
			.then(isLoggedIn => {
				if (isLoggedIn) {
					try {
						action();
					} catch (e) {
						log.error(`Error handling menu action ${e}`);
					}
				} else {
					log.info('Can\'t handle logged only  action');
				}
			});
	};
}

class ApplicationMenu {

	constructor() {
		this.isPlaying = false;
		this.isFamily = false;
		this.loggedIn = false;
		this.canGoBack = false;
		this.canGoForward = false;
		this.shuffleEnabled = false;
		this.repeatMode = 0;
		this.debouncedSetMenu = false;
	}

	setupStatusListeners() {
		LoginInterface.on('user-is-logged-in', () => {
			this.loggedIn = true;
			this.setMenu();
		});
		LoginInterface.on('user-is-logged-out', () => {
			this.loggedIn = false;
			this.setMenu();
		});

		HistoryInterface.on('update-data', ({canGoBack, canGoForward}) => {
			this.canGoBack = canGoBack;
			this.canGoForward = canGoForward;
			this.setMenu();
		});

		DzPlayerInterface.on('shuffle-changed', value => {
			this.shuffleEnabled = value;
			this.setMenu();
		});

		DzPlayerInterface.on('repeat-changed', value => {
			this.repeatMode = value;
			this.setMenu();
		});

		DzPlayerInterface.on('playing-changed', value => {
			this.isPlaying = value;
			this.setMenu();
		});
	}

	getPreferencesEntry() {
		const AppMenu = this;
		return {
			label: t('menu_preferences_label') || 'Preferences',
			enabled: AppMenu.loggedIn,
			click() {
				triggerIfLogged(() => {
					HistoryInterface.goTo('/account');
				})();
			}
		};
	}

	getSwitchProfileEntry() {
		const AppMenu = this;
		return {
			label: t('menu_switch-profile_label') || 'Switch Profile',
			enabled: AppMenu.loggedIn,
			click() {
				triggerIfLogged(() => {
					LoginInterface.switchProfile();
				})();
			}
		};
	}

	getOfflineModeEntry() {
		return {
			label: t('menu_offline-mode_label') || 'Offline mode',
			enabled: false
		};
	}

	getLogoutEntry() {
		const AppMenu = this;
		return {
			label: t('menu_log-out_label') || 'Log out',
			enabled: AppMenu.loggedIn,
			click() {
				triggerIfLogged(() => {
					LoginInterface.triggerLogout();
				})();
			}
		};
	}

	getCreatePlaylistEntry() {
		return {
			label: t('menu_create-playlist_label') || 'Create Playlist',
			enabled: false
		};
	}

	getImportMusicFromItunes() {
		return {
			label: t('menu_import-from-itunes_label') || 'Import music from iTunes',
			enabled: false
		};
	}

	getFileMenuCommonElements() {
		const fileMenuCommonElements = [];

		fileMenuCommonElements.push(this.getPreferencesEntry());
		fileMenuCommonElements.push({type: 'separator'});
		if (this.isFamily) {
			fileMenuCommonElements.push(this.getSwitchProfileEntry());
		}
		if (this.loggedIn) {
			fileMenuCommonElements.push(this.getOfflineModeEntry());
			fileMenuCommonElements.push({type: 'separator'});
			fileMenuCommonElements.push(this.getLogoutEntry());
		}

		return fileMenuCommonElements;
	}

	getMacOSXMenuTemplate() {
		const extraMenuComponents = Utils.isMac() ? this.getFileMenuCommonElements() : [];
		return {
			label: app.getName(),
			submenu: [
				{
					label: t('menu_about_label') || 'About',
					role: 'about'
				}, {
					type: 'separator'
				},
				...extraMenuComponents, {
					type: 'separator'
				}, {
					label: t('menu_hide-deezer_label') || 'Hide Deezer',
					role: 'hide'
				}, {
					label: t('menu_hide-others_label') || 'Hide others',
					role: 'hideothers'
				}, {
					label: t('menu_show-all_label') || 'Show all',
					role: 'unhide'
				}, {
					type: 'separator'
				}, {
					label: t('menu_quit-deezer_label') || 'Quit Deezer',
					role: 'quit'
				}
			]
		};
	}

	getFileMenuTemplate() {

		const extraMenuComponents = Utils.isMac() ? [] : this.getFileMenuCommonElements();
		const fileMenuTemplate = {
			label: t('menu_file_label') || 'File',
			'submenu': [
				this.getCreatePlaylistEntry(),
				// this.getImportMusicFromItunes(),
				...extraMenuComponents
			]
		};


		return fileMenuTemplate;
	}

	getEditMenuTemplate() {
		return {
			label: t('menu_edit_label') || 'Edit',
			submenu: [
				{
					label: t('menu_undo_label') || 'Undo',
					role: 'undo'
				}, {
					label: t('menu_redo_label') || 'Redo',
					role: 'redo'
				}, {
					type: 'separator'
				}, {
					label: t('menu_cut_label') || 'Cut',
					role: 'cut'
				}, {
					label: t('menu_copy_label') || 'Copy',
					role: 'copy'
				}, {
					label: t('menu_paste_label') || 'Paste',
					role: 'paste'
				}, {
					label: t('menu_select-all_label') || 'Select all',
					role: 'selectall'
				}
			]
		};
	}

	getViewMenuTemplate() {
		const template = {
			label: t('menu_view_label') || 'View',
			submenu: [
				{
					label: t('menu_zoom-in_label') || 'Zoom in',
					role: 'zoomin'
				}, {
					label: t('menu_zoom-out_label') || 'Zoom out',
					role: 'zoomout'
				}, {
					label: t('menu_actual-size_label') || 'Actual size',
					role: 'resetzoom'
				}, {
					type: 'separator'
				}, {
					label: t('menu_go-back_label') || 'Go back',
					enabled: this.canGoBack,
					click() {
						triggerIfLogged(() => {
							HistoryInterface.goBack();
						})();
					}
				}, {
					label: t('menu_go-forward_label') || 'Go forward',
					enabled: this.canGoForward,
					click() {
						triggerIfLogged(() => {
							HistoryInterface.goForward();
						})();
					}
				}, {
					type: 'separator'
				}, {
					label: t('menu_enter-full-screen_label') || 'Enter full screen',
					role: 'togglefullscreen'
				}
			]
		};

		if (Utils.hasDevTools()) {
			template.submenu.unshift({
				type: 'separator'
			});

			template.submenu.unshift({
				role: 'toggledevtools'
			});

			template.submenu.unshift({
				label: 'Clear AppData and restart',
				click() {
					AppInterface.clearAppDataAndRestart();
				}
			});
		}
		return template;
	}

	getPlayMenuTemplate() {
		const AppMenu = this;
		return {
			label: t('menu_play_label') || 'Play',
			submenu: [
				{
					label: this.isPlaying ? (t('menu_pause_label') || 'Pause') : (t('menu_play_label') || 'Play'),
					enabled: AppMenu.loggedIn,
					// accelerator: 'space',
					click() {
						triggerIfLogged(() => {
							DzPlayerInterface.togglePause();
						})();
					}
				}, {
					label: t('menu_next_label') || 'Next',
					accelerator: 'CmdOrCtrl+Right',
					enabled: AppMenu.loggedIn,
					click() {
						triggerIfLogged(() => {
							DzPlayerInterface.next();
						})();
					}
				}, {
					label: t('menu_previous_label') || 'Previous',
					accelerator: 'CmdOrCtrl+Left',
					enabled: AppMenu.loggedIn,
					click() {
						triggerIfLogged(() => {
							DzPlayerInterface.prev();
						})();
					}
				}, {
					type: 'separator'
				}, {
					label: t('menu_shuffle_label') || 'Shuffle',
					enabled: AppMenu.loggedIn,
					submenu: [
						{
							label: t('menu_on_label') || 'On',
							type: 'radio',
							checked: AppMenu.shuffleEnabled,
							enabled: AppMenu.loggedIn,
							click() {
								if (!AppMenu.shuffleEnabled) {
									triggerIfLogged(() => {
										DzPlayerInterface.enableShuffle();
									})();
								}
							}
						},
						{
							label: t('menu_off_label') || 'Off',
							type: 'radio',
							checked: !AppMenu.shuffleEnabled,
							enabled: AppMenu.loggedIn,
							click() {
								if (AppMenu.shuffleEnabled) {
									triggerIfLogged(() => {
										DzPlayerInterface.disableShuffle();
									})();
								}
							}
						}
					]
				},{
					label: t('menu_repeat_label') || 'Repeat',
					enabled: AppMenu.loggedIn,
					submenu: [
						{
							label: t('menu_off_label') || 'Off',
							type: 'radio',
							checked: AppMenu.repeatMode === 0,
							enabled: AppMenu.loggedIn,
							click() {
								if (AppMenu.repeatMode !== 0) {
									triggerIfLogged(() => {
										DzPlayerInterface.setRepeatMode(0);
									})();
								}
							}
						},
						{
							label: t('menu_all_label') || 'All',
							type: 'radio',
							checked: AppMenu.repeatMode === 1,
							enabled: AppMenu.loggedIn,
							click() {
								if (AppMenu.repeatMode !== 1) {
									triggerIfLogged(() => {
										DzPlayerInterface.setRepeatMode(1);
									})();
								}
							}
						},
						{
							label: t('menu_one_label') || 'One',
							type: 'radio',
							checked: AppMenu.repeatMode === 2,
							enabled: AppMenu.loggedIn,
							click() {
								if (AppMenu.repeatMode !== 2) {
									triggerIfLogged(() => {
										DzPlayerInterface.setRepeatMode(2);
									})();
								}
							}
						}
					]
				}, {
					type: 'separator'
				}, {
					label: t('menu_volume-up_label') || 'Volume Up',
					accelerator: 'CmdOrCtrl+Up',
					enabled: AppMenu.loggedIn,
					click() {
						triggerIfLogged(() => {
							DzPlayerInterface.increaseVolume();
						})();
					}
				}, {
					label: t('menu_volume-down_label') || 'Volume Down',
					accelerator: 'CmdOrCtrl+Down',
					enabled: AppMenu.loggedIn,
					click() {
						triggerIfLogged(() => {
							DzPlayerInterface.decreaseVolume();
						})();
					}
				}
			]
		};
	}

	getWindowMenuTemplate() {
		return {
			role: 'window',
			label: t('menu_window_label') || 'Window',
			submenu: [
				{
					label: t('menu_minimize_label') || 'Minimize',
					role: 'minimize'
				}, {
					label: t('menu_close_label') || 'Close',
					role: 'close'
				}, {
					type: 'separator'
				}, {
					label: t('menu_bring-front_label') || 'Bring all to front',
					role: 'front'
				}
			]
		};
	}

	getHelpMenuTemplate() {
		return {
			role: 'help',
			label: t('menu_help_label') || 'Help',
			submenu: [
				{
					label: `Deezer ${app.getVersion()}`
				}, {
					label: t('menu_about_label') || 'About',
					click() {
						require('electron').shell.openExternal('https://www.deezer.com/features');
					}
				}
			]
		};
	}

	getMenuTemplate() {

		let template = [];
		template = [
			this.getFileMenuTemplate(),
			this.getEditMenuTemplate(),
			this.getViewMenuTemplate(),
			this.getPlayMenuTemplate(),
			this.getWindowMenuTemplate(),
			this.getHelpMenuTemplate()
		];

		if (process.platform === 'darwin') {
			template.unshift(this.getMacOSXMenuTemplate());
		}

		return template;
	}

	setMenu() {
		if (this.debouncedSetMenu === false) {
			this.debouncedSetMenu = debounce(() => {
				LoginInterface
					.isFamily()
					.then(isFamily => {
						this.isFamily = isFamily;
						this._setMenu();
					})
					.catch(() => {
						this.isFamily = false;
						this._setMenu();
					});

			}, 200);
		}
		this.debouncedSetMenu();
	}

	_setMenu() {
		const menuTemplate = this.getMenuTemplate(this.loggedIn);
		const menu = Menu.buildFromTemplate(menuTemplate);
		Menu.setApplicationMenu(menu);
		if (this.loggedIn) {
			globalShortcut.register('medianexttrack', triggerIfLogged(DzPlayerInterface.next));
			globalShortcut.register('mediaprevioustrack', triggerIfLogged(DzPlayerInterface.prev));
			globalShortcut.register('mediaplaypause', triggerIfLogged(DzPlayerInterface.togglePause));
			globalShortcut.register('mediastop', triggerIfLogged(DzPlayerInterface.stop));
		} else {
			globalShortcut.unregisterAll();
		}
	}
}

export default new ApplicationMenu();
