import dns from 'dns';
import os from 'os';
import crypto from 'crypto';
import debounce from 'lodash.debounce';
import ConnectivityInterface from '../../common/IpcInterface/ConnectivityInterface';

let connected = false;
let interval = false;

class ConnectivityStatus {
	constructor() {

		this.debouncedEmitChangedNetwork = debounce(() => {
			ConnectivityInterface.emit('networks-list-changed');
		}, 2000);

		this.updateNetworkList();
		const checkConnectivity = () => {
			dns.lookup('www.deezer.com', (err) => {
				const wasConnected = connected;
				connected = !(err && err.code == 'ENOTFOUND');
				if (connected !== wasConnected) {
					ConnectivityInterface.emit(connected ? 'connectivity-available' : 'connectivity-not-available');
				}
			});
			const currentNetworkListChecksum = this.currentNetworkListChecksum;
			this.updateNetworkList();
			if (currentNetworkListChecksum !== this.currentNetworkListChecksum) {
				this.debouncedEmitChangedNetwork();
			}
		};

		clearInterval(interval);
		checkConnectivity();
		interval = setInterval(checkConnectivity, 5 * 1000);
	}

	updateNetworkList() {
		this.currentNetworkListChecksum = crypto.createHash('md5').update(JSON.stringify(os.networkInterfaces())).digest('hex');
	}

	getConnectivityStatus() {
		return connected;
	}
}

export default new ConnectivityStatus();
