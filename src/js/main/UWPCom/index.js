import {app} from 'electron';
import log from 'electron-log';
import path from 'path';
import fs from 'fs';
import uuid from 'uuid/v4';
import Utils from '../Utils';
import DzPlayerInterface from '../../common/IpcInterface/DzPlayerInterface';
import LoginInterface from '../../common/IpcInterface/LoginInterface';

const SERVICE_SMTC = 'smtc';
const ACTION_SMTC_UPDATE = 'update';
const ACTION_SMTC_RESET = 'reset';
const TYPE_REQUEST = 'request';
const TYPE_RESPONSE = 'response';

class UWPCom {

	constructor() {
		this.started = false;
		this.spawnedProcess = false;
		this.loggedIn = false;

		if (Utils.isWindows()) {
			this.UWPBridgeExecutable = path.join(app.getPath('exe'), '..', 'WindowsAppService.exe');
			this.startup();

			DzPlayerInterface.on('playing-changed', value => {
				this.isPlaying = value;
				this.updateCurrentTrackStatus();
			});
			DzPlayerInterface.on('current-track-changed', value => {
				this.currentTrackData = value;
				this.updateCurrentTrackStatus();
			});

			LoginInterface.on('user-is-logged-in', () => {
				this.loggedIn = true;
				this.updateCurrentTrackStatus();
			});
			LoginInterface.on('user-is-logged-out', () => {
				this.loggedIn = false;
				this.resetCurrentTrackStatus();
			});
		}
	}

	startup() {
		const spawn = require('child_process').spawn;

		fs.lstat(this.UWPBridgeExecutable, (err, stats) => {
			if (err) {
				log.info('UWP Bridge is not available');
			}

			if (stats && stats.isFile()) {
				log.info('Registering background tasks');

				this.spawnedProcess = spawn(this.UWPBridgeExecutable);
				this.spawnedProcess.stdout.on('data', (data) => {
					log.debug(`stdout: ${data}`);
					try {
						const parsedData = JSON.parse(data);
						if (parsedData.type === TYPE_REQUEST) {
							this.handleCommand(parsedData);
						}
					} catch (e) {
						log.error(`Could not interpret command ${data}`);
					}
				});

				this.spawnedProcess.stderr.on('data', (data) => {
					log.debug(`stderr: ${data}`);
				});

				this.spawnedProcess.on('close', (code) => {
					log.warn(`child process exited with code ${code}`);
					this.started = false;
				});
				this.started = true;
			} else {
				log.warn('BackgroundTaskRegisterer not found.');
			}
		});
	}

	handleCommand(command) {
		switch (command.service) {
			case SERVICE_SMTC:
				this.handleSTMCServiceCommand(command);
				break;
			default:
				log.error(`Unkown service ${command.service}`);
				break;
		}
	}

	handleSTMCServiceCommand(command) {
		switch (command.action) {
			case 'playback':
				this.handleSMTCServicePlaybackCommand(command);
				break;
			default:
				log.error(`Unkown action ${command.service} ${command.action}`);
				break;
		}
	}

	handleSMTCServicePlaybackCommand(command) {
		switch (command.data.command.toLowerCase()) {
			case 'play':
				this.handleLoggedPlaybackCommand(() => {
					if (!this.isPlaying) {
						DzPlayerInterface.togglePause();
					}
				});
				break;
			case 'pause':
				this.handleLoggedPlaybackCommand(() => {
					if (this.isPlaying) {
						DzPlayerInterface.togglePause();
					}
				});
				break;
			case 'next':
				this.handleLoggedPlaybackCommand(() => {
					DzPlayerInterface.next();
				});
				break;
			case 'previous':
				this.handleLoggedPlaybackCommand(() => {
					DzPlayerInterface.prev();
				});
				break;
			case 'activate':
				app.focus();
				break;
			default:
				log.error(`Unkown playback command ${command.service} ${command.action} ${command.data.command}`);
				break;
		}
		this.writeCommand(this.createResponse(command.requestId, true, {}));
	}

	handleLoggedPlaybackCommand(action) {
		if (this.loggedIn) {
			return action();
		} else {
			log.warn('Use not logged not executing UWP playback control command');
		}
	}

	writeCommand(data) {
		if (this.started === true) {
			log.debug('Writing command to pipe', data);
			const command = `${JSON.stringify(data)}\n`;
			this.spawnedProcess.stdin.write(command);
		} else {
			log.info('UWP Communication process is not running');
		}
	}

	createCommand(service, action, data) {
		return {type: TYPE_REQUEST, requestId: uuid(), service, action, data};
	}

	createResponse(requestId, success, data) {
		return {type: TYPE_RESPONSE, requestId: uuid(), success, data};
	}

	updateCurrentTrackStatus() {
		if (this.started && this.currentTrackData && this.currentTrackData.title) {
			const command = this.createCommand(SERVICE_SMTC, ACTION_SMTC_UPDATE, {
				title: this.currentTrackData.title || '',
				artist: this.currentTrackData.artist || '',
				album: this.currentTrackData.album || '',
				cover: this.currentTrackData.cover || '',
				isPlaying: this.isPlaying,
				isPlayEnabled: true,
				isPauseEnabled: true,
				isPrevEnabled: true,
				isNextEnabled: true
			});
			this.writeCommand(command);
		}
	}

	resetCurrentTrackStatus() {
		this.writeCommand(this.createCommand(SERVICE_SMTC, ACTION_SMTC_RESET, {}));
	}
}

export default new UWPCom();
