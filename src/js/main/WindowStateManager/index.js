import settings from 'electron-settings';
import log from 'electron-log';
import {screen} from 'electron';

const WINDOW_STATE_DATA_SETTING_KEY = 'windowStateData';
const DEFAULT_WIDTH = 990;
const DEFAULT_HEIGHT = 600;

export default class WindowStateManager {
	constructor(app) {
		this.app = app;
	}

	saveState() {
		const appPosition = this.app.getWindow().getPosition();
		const displays = screen.getAllDisplays();
		let currentDisplayId = false;

		for (var i = 0; i < displays.length; i++) {
			const display = displays[i];
			if (appPosition[0] >= display.bounds.x && appPosition[0] < (display.bounds.x + display.bounds.width)) {
				currentDisplayId = display.id;
				break;
			}
		}

		const dataToSave = {
			maximized: this.app.getWindow().isMaximized(),
			pos: [
				Math.max(0, appPosition[0]),
				Math.max(1, appPosition[1])
			],
			currentDisplayId: currentDisplayId,
			size: this.app.getWindow().getSize()
		};
		log.debug('Saving window position data', dataToSave);
		settings.set(WINDOW_STATE_DATA_SETTING_KEY, dataToSave);
	}

	getState() {
		const displays = screen.getAllDisplays();
		const primaryDisplay = screen.getPrimaryDisplay();

		let defaultWidth = DEFAULT_WIDTH;
		let defaultHeight = DEFAULT_HEIGHT;

		if (primaryDisplay.bounds.width >= 1280) {
			defaultWidth = 1210;
			defaultHeight = 640;
		}
		if (primaryDisplay.bounds.width >= 1920) {
			defaultWidth = 1210;
			defaultHeight = 700;
		}

		let state = {
			width: defaultWidth,
			height: defaultHeight,
			maximized: false,
			currentDisplayId: false
		};

		if (settings.has(WINDOW_STATE_DATA_SETTING_KEY)) {
			const savedData = settings.get(WINDOW_STATE_DATA_SETTING_KEY);
			state = {
				x: savedData.pos[0],
				y: savedData.pos[1],
				width: savedData.size[0],
				height: savedData.size[1],
				currentDisplayId: savedData.currentDisplayId,
				maximized: savedData.maximized
			};
		}

		log.info('Precheck state', state);

		let isOutOfBounds = true;

		for (var i = 0; i < displays.length; i++) {
			const display = displays[i];
			let xFit = false;
			let yFit = false;
			if (state.x >= display.workArea.x && state.x <= (display.workArea.x + display.workArea.width)) {
				xFit = true;
			}

			if (state.y >= display.workArea.y && state.y <= (display.workArea.y + display.workArea.height)) {
				yFit = true;
			}

			log.debug(
				display.id,
				'xFit', xFit,
				'yFit', yFit,
				'currentScreen',
				display.id === state.currentDisplayId ? 'true' : 'false');

			if (xFit && yFit && display.id === state.currentDisplayId) {
				isOutOfBounds = false;
				break;
			}
		}

		if (isOutOfBounds) {
			log.warn('Saved position is out of bounds, reset to appear centered primary display');
			delete state.x;
			delete state.y;
		}

		log.debug('Applying window position data', state);
		return state;
	}
}
