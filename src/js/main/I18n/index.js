import {app, session} from 'electron';
import log from 'electron-log';
import i18next from 'i18next';
import sprintf from 'i18next-sprintf-postprocessor';
import {pathExistsSync} from 'fs-extra';
import path from 'path';

export const ELECTRON_LOCALE_TO_DZ_LOCALE = {
	// 'af': '', //Afrikaans
	// 'am': '', //Amharic
	// 'ar': 'ar_EG', //Arabic // TODO Handle RTL
	// 'az': '', //Azerbaijani
	// 'be': '', //Belarusian
	// 'bg': '', //Bulgarian
	// 'bh': '', //Bihari
	// 'bn': '', //Bengali
	// 'br': '', //Breton
	// 'bs': '', //Bosnian
	// 'ca': '', //Catalan
	// 'co': '', //Corsican
	'cs': 'cs_CZ', //Czech
	// 'cy': '', //Welsh
	'da': 'da_DK', //Danish
	'de': 'de_DE', //German
	'de-AT': 'de_DE', //German (Austria)
	'de-CH': 'de_DE', //German (Switzerland)
	'de-DE': 'de_DE', //German (Germany)
	// 'el': '', //Greek
	'en': 'en_GB', //English
	'en-AU': 'en_GB', //English (Australia)
	'en-CA': 'en_US', //English (Canada)
	'en-GB': 'en_GB', //English (UK)
	'en-NZ': 'en_GB', //English (New Zealand)
	'en-US': 'en_US', //English (US)
	'en-ZA': 'en_GB', //English (South Africa)
	// 'eo': '', //Esperanto
	'es': 'es_ES', //Spanish
	'es-419': 'es_ES', //Spanish (Latin America)
	// 'et': '', //Estonian
	// 'eu': '', //Basque
	// 'fa': '', //Persian
	'fi': 'fi_FI', //Finnish
	// 'fil': '', //Filipino
	// 'fo': '', //Faroese
	'fr': 'fr_FR', //French
	'fr-CA': 'fr_FR', //French (Canada)
	'fr-CH': 'fr_FR', //French (Switzerland)
	'fr-FR': 'fr_FR', //French (France)
	// 'fy': '', //Frisian
	// 'ga': '', //Irish
	// 'gd': '', //Scots Gaelic
	// 'gl': '', //Galician
	// 'gn': '', //Guarani
	// 'gu': '', //Gujarati
	// 'ha': '', //Hausa
	// 'haw': '', //Hawaiian
	// 'he': '', //Hebrew
	// 'hi': '', //Hindi
	'hr': 'hr_HR', //Croatian
	'hu': 'hu_HU', //Hungarian
	// 'hy': '', //Armenian
	// 'ia': '', //Interlingua
	'id': 'id_ID', //Indonesian
	// 'is': '', //Icelandic
	'it': 'it_IT', //Italian
	'it-CH': 'it_IT', //Italian (Switzerland)
	'it-IT': 'it_IT', //Italian (Italy)
	'ja': 'ja_JP', //Japanese
	// 'jw': '', //Javanese
	// 'ka': '', //Georgian
	// 'kk': '', //Kazakh
	// 'km': '', //Cambodian
	// 'kn': '', //Kannada
	'ko': 'ko_KR', //Korean
	// 'ku': '', //Kurdish
	// 'ky': '', //Kyrgyz
	// 'la': '', //Latin
	// 'ln': '', //Lingala
	// 'lo': '', //Laothian
	// 'lt': '', //Lithuanian
	// 'lv': '', //Latvian
	// 'mk': '', //Macedonian
	// 'ml': '', //Malayalam
	// 'mn': '', //Mongolian
	// 'mo': '', //Moldavian
	// 'mr': '', //Marathi
	// 'ms': '', //Malay
	// 'mt': '', //Maltese
	'nb': 'nb_NO', //Norwegian (Bokmal)
	'ne': '', //Nepali
	'nl': 'nl_NL', //Dutch
	'nn': 'nb_NO', //Norwegian (Nynorsk)
	'no': 'nb_NO', //Norwegian
	// 'oc': '', //Occitan
	// 'om': '', //Oromo
	// 'or': '', //Oriya
	// 'pa': '', //Punjabi
	'pl': 'pl_PL', //Polish
	// 'ps': '', //Pashto
	'pt': 'pt_PT', //Portuguese
	'pt-BR': 'pt_BR', //Portuguese (Brazil)
	'pt-PT': 'pt_PT', //Portuguese (Portugal)
	// 'qu': '', //Quechua
	// 'rm': '', //Romansh
	'ro': 'ro_RO', //Romanian
	'ru': 'ru_RU', //Russian
	// 'sd': '', //Sindhi
	// 'sh': '', //Serbo-Croatian
	// 'si': '', //Sinhalese
	'sk': 'sk_SK', //Slovak
	'sl': 'sl_SI', //Slovenian
	// 'sn': '', //Shona
	// 'so': '', //Somali
	'sq': 'sq_AL', //Albanian
	'sr': 'sr_RS', //Serbian
	// 'st': '', //Sesotho
	// 'su': '', //Sundanese
	'sv': 'sv_SE', //Swedish
	// 'sw': '', //Swahili
	// 'ta': '', //Tamil
	// 'te': '', //Telugu
	// 'tg': '', //Tajik
	// 'th': '', //Thai
	// 'ti': '', //Tigrinya
	// 'tk': '', //Turkmen
	// 'to': '', //Tonga
	'tr': 'tr_TR', //Turkish
	// 'tt': '', //Tatar
	// 'tw': '', //Twi
	// 'ug': '', //Uighur
	'uk': 'uk_UA', //Ukrainian
	// 'ur': '', //Urdu
	// 'uz': '', //Uzbek
	// 'vi': '', //Vietnamese
	// 'xh': '', //Xhosa
	// 'yi': '', //Yiddish
	// 'yo': '', //Yoruba
	'zh': 'zh_CN', //Chinese
	'zh-CN': 'zh_CN', //Chinese (Simplified)
	'zh-TW': 'zh_CN', //Chinese (Traditional)
	// 'zu': '', //Zulu
};

export const DZ_LOCALE_TO_LANGUAGE = {
	'en_GB': 'en',
	'en_US': 'us',
	'fi_FI': 'fi',
	'fr_FR': 'fr',
	'da_DK': 'da',
	'de_DE': 'de',
	'es_ES': 'es',
	'it_IT': 'it',
	'nl_NL': 'nl',
	'pt_PT': 'pt',
	'ru_RU': 'ru',
	'ko_KR': 'ko',
	'pt_BR': 'br',
	'id_ID': 'id',
	'pl_PL': 'pl',
	'tr_TR': 'tr',
	'ro_RO': 'ro',
	'ms_MY': 'ms',
	'hu_HU': 'hu',
	'sr_RS': 'rs',
	'sq_AL': 'sq',
	'th_TH': 'th',
	'zh_CN': 'cn',
	'ar_EG': 'ar',
	'hr_HR': 'hr',
	'es_MX': 'mx',
	'cs_CZ': 'cs',
	'sk_SK': 'sk',
	'sl_SI': 'sl',
	'sv_SE': 'se',
	'nb_NO': 'no',
	'ja_JP': 'ja',
	'uk_UA': 'uk'
};

export function saveLocaleToCookie() {
	return new Promise((resolve) => {
		const cookie = {
			url: 'https://www.deezer.com',
			name: 'dz_lang',
			value: `${getDzLocaleFromLocale(app.getLocale())}`,
			domain: '.deezer.com',
			hostOnly: false,
			path: '/',
			secure: false,
			httpOnly: true
		};
		session.defaultSession.cookies.set(cookie, (error) => {
			if (error) {
				log.error(`${error} while saving locale to cookie`);
			} else {
				log.debug(`Saved locale ${getDzLocaleFromLocale(app.getLocale())} to dz_lang cookie`);
			}
			session.defaultSession.clearCache(resolve);
		});
	});
}

export function getDzLangFromLocale(locale) {
	return ELECTRON_LOCALE_TO_DZ_LOCALE[locale] || 'en_US';
}

export function getDzLocaleFromLocale(locale) {
	return DZ_LOCALE_TO_LANGUAGE[getDzLangFromLocale(locale)] || 'en';
}

function getLangPath(lang) {
	return path.join(__dirname, 'locales', `translations.${lang}.json`);
}

export function init() {
	let targetLang = getDzLocaleFromLocale(app.getLocale());
	let targetPath = getLangPath(targetLang);
	if (!pathExistsSync(targetPath)) {
		targetLang = 'en';
		targetPath = getLangPath(targetLang);
	}
	const resources = {};
	resources[targetLang] = {
		translation: require(targetPath)
	};
	i18next
		.use(sprintf)
		.init({
			debug: false,
			resources,
			lng: targetLang,
			fallbackLng: false,
			lowerCaseLng: true,
			keySeparator: '::',
			nsSeparator: ':::',
			postProcess: 'sprintf'
		});
}

export function t(keys, options) {
	return i18next.t(keys, options);
}
