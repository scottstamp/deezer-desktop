import LoginInterface from '../../common/IpcInterface/LoginInterface';
import AppInterface from '../../common/IpcInterface/AppInterface';

class Sentry {
	constructor() {
		this.client = false;
		this.shouldSend = Math.random() > 0.08;
		this.userId = 0;
		this.isEmployee = false;
		this.offerId = -1;
		this.country = 'N/A';

		LoginInterface.on('user-is-logged-in', () => {
			this.updateUserData().then(() => {
				this.updateSentryClient();
			});
		});
		LoginInterface.on('user-is-logged-out', () => {
			this.updateUserData().then(() => {
				this.updateSentryClient();
			});
		});
	}

	setClient(client){
		this.client = client;
		this.updateUserData().then(() => {
			this.updateSentryClient();
		});
	}

	updateUserData() {
		return Promise.all([
			LoginInterface.gatekeepIsAllowed('sentry'),
			LoginInterface.getUserId(),
			LoginInterface.isEmployee(),
			LoginInterface.getOfferId(),
			LoginInterface.getCountry(),
			AppInterface.getSystemSentryTags()
		]).then(results => {
			this.shouldSend = results[0];
			this.userId = results[1];
			this.isEmployee = results[2];
			this.offerId = results[3];
			this.country = results[4];
			this.sentryTags = results[5];
			return Promise.resolve();
		});
	}

	updateSentryClient() {
		const tags = {
			country: this.country,
			offerId: this.offerId,
			...this.sentryTags
		};
		const user = {
			id: this.userId,
			isEmployeeUser: this.isEmployee
		};
		this.client.setContext({tags, user});
	}
}

export default new Sentry();
