import {app} from 'electron';
import {autoUpdater} from 'electron-updater';
import log from 'electron-log';
import os from 'os';
import url from 'url';
import Utils from '../Utils';
import AutoUpdateInterface from '../../common/IpcInterface/AutoUpdateInterface';
import LoginInterface from '../../common/IpcInterface/LoginInterface';

const platform = os.platform();
const currentAppVersion = app.getVersion();

class AutoUpdater {
	constructor() {
		this.updateDisabled = Utils.hasNoUpdateFlag();
		this.lastUpdateCheck = 0;
		this.arch = 'x64';

		switch (os.arch()) {
			case 'ia32':
			case 'x32':
			case 'x86':
				this.arch = 'x86';
				break;
			case 'x64':
				this.arch = 'x64';
				break;
			default:
				log.error(`Platform ${os.arch()}is not known`);
		}

		autoUpdater.autoDownload = true;
		autoUpdater.logger = log;
		autoUpdater.on('error', (error) => {
			log.info('Error in autoupdate', error);
			AutoUpdateInterface.emit('error', error);
		});

		autoUpdater.on('checking-for-update', () => {
			log.info('Checking for update');
			AutoUpdateInterface.emit('checking-for-update');
		});

		autoUpdater.on('update-available', (info) => {
			log.info('Update available', info);
			AutoUpdateInterface.emit('update-available', info);
		});

		autoUpdater.on('update-not-available', () => {
			log.info('Update not available');
			AutoUpdateInterface.emit('update-not-available');
		});

		autoUpdater.on('download-progress', (progress) => {
			log.debug('Download progress', progress);
			AutoUpdateInterface.emit('download-progress', progress);
		});

		autoUpdater.on('update-downloaded', (info) => {
			log.info('Update downloaded', info);
			AutoUpdateInterface.emit('update-downloaded', info);
		});

		setTimeout(() => {
			this.checkForUpdates();
		}, 20 * 1000);
		setInterval(() => {
			this.checkForUpdates();
		}, 5 * 60 * 1000);
	}

	setUpdateTarget() {
		return new Promise(resolve => {

			Promise.all([LoginInterface.isEmployee(), LoginInterface.getUserId()]).then(results => {
				const isEmployee = results[0];
				const userId = results[1];

				const urlParams = {
					pathname: '/desktop/update',
					host: 'www.deezer.com',
					protocol: 'https:',
					slashes: true,
					query: {
						currentVersion: currentAppVersion,
						architecture: this.arch,
						platform: platform,
						platformVersion: Utils.getPlatformVersion(),
						employee: isEmployee
							? 'yes'
							: 'no'
					}
				};

				if (userId) {
					urlParams.query.userId = userId;
				}

				const updateUrl = url.format(urlParams);

				log.info('Setting feed URL: ', updateUrl);
				autoUpdater.setFeedURL(updateUrl);
				resolve();
			});

		});
	}

	checkForUpdates() {
		if (this.updateDisabled) {
			log.warn('Update has been disabled with flag, not checking');
			return;
		}
		const now = Date.now();
		const elapsed = now - this.lastUpdateCheck;
		if (elapsed > (6 * 3600 * 1000)) {
			this.lastUpdateCheck = Date.now();
			log.debug('Setting update target');
			this.setUpdateTarget().then(() => {
				log.info('Checking for update');
				autoUpdater.checkForUpdates();
			});
			return;
		}

		log.debug('Checked for update less than 6 hours ago');
	}
}

export default new AutoUpdater();
