import isRenderer from 'is-electron-renderer';
import os from 'os';
import macosVersion from 'macos-version';
import isDev from 'electron-is-dev';

const platformVersion = process.platform === 'darwin' ? macosVersion() : os.release();

export default class Utils {

	static isMain() {
		return !isRenderer;
	}

	static isRenderer() {
		return isRenderer;
	}

	static isMac() {
		return process.platform === 'darwin';
	}

	static isWindows() {
		return process.platform === 'win32';
	}

	static isLinux() {
		return process.platform === 'linux';
	}

	static isProd() {
		return (process.env.DEEZER_DESKTOP_ENV || 'production') === 'production';
	}

	static isElectronConnect() {
		return process.env.ELECTRON_CONNECT === 'on';
	}

	static getPlatformVersion() {
		return platformVersion;
	}

	static getLogFormattedOsName() {
		if (this.isMac()) {
			return 'osx';
		}
		if (this.isWindows()) {
			return 'windows';
		}
		if (this.isLinux()) {
			return 'linux';
		}
		return 'unknown';
	}

	static hasDevTools() {
		return isDev || process.env.DZ_DEVTOOLS === 'yes';
	}

	static hasNoUpdateFlag () {
		return process.env.DZ_DISABLE_UPDATE === 'yes';
	}
}
