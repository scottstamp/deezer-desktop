// Keep the call to all interfaces, allows them to be setup
import interfaces from '../common/IpcInterface/interfaces';
import {getDzLangFromLocale, getDzLocaleFromLocale} from '../main/I18n';
import log from 'electron-log';
const remote = require('electron').remote;
const Utils = remote.require('./main/Utils').default;

const flags = {
	preventWindowClose: navigator.platform === 'MacIntel',
	allowWindowReload: false,
	forceTriforceAllowed: false
};

const electronRenderer = {
	basePath: __dirname,
	player: interfaces.NativePlayerInterface,
	app: interfaces.AppInterface,
	login: interfaces.LoginInterface,
	dzPlayer: interfaces.DzPlayerInterface,
	history: interfaces.HistoryInterface,
	autoStart: interfaces.AutoStartInterface,
	storage: interfaces.DataStorageInterface,
	connectivity: interfaces.ConnectivityInterface,
	flags: flags,
	store: false,
	config: false,
	reloadApp: () => {
		flags.preventWindowClose = false;
		flags.allowWindowReload = true;
		interfaces.LoginInterface.login();
	},
	deleteLoginInfo: (reloadAfter = true) => {
		flags.preventWindowClose = false;
		flags.allowWindowReload = true;
		interfaces.LoginInterface.logout(reloadAfter);
	},
	restartForUpdate: () => {
		flags.preventWindowClose = false;
		flags.allowWindowReload = false;
		interfaces.AutoUpdateInterface.restartForUpdate();
	},
	originalUserAgent: '',
	onFacebookLoginOk: () => {},
	getDzLangFromLocale: getDzLangFromLocale,
	getDzLocaleFromLocale: getDzLocaleFromLocale,
	version: require('electron').remote.app.getVersion(),
	sentryTags: {},
	platformObjectProps: {
		os_name: Utils.isMac() ? 'osx' : (Utils.isWindows() ? 'win' : 'linux'),
		os_version: Utils.isMac() ? remote.require('macos-version')() : remote.require('os').release(),
		lang: getDzLocaleFromLocale(navigator.language),
	}
};

interfaces.AppInterface.getOriginalUserAgent().then(value => {
	electronRenderer.originalUserAgent = value;
});
interfaces.AppInterface.hasTriforceFlag().then(value => {
	flags.forceTriforceAllowed = value;
});
interfaces.AppInterface.hasDevEnv().then(value => {
	flags.hasDevEnv = value;
});
interfaces.AppInterface.getSystemSentryTags().then(tags => {
	electronRenderer.sentryTags = tags;
});

interfaces.AutoUpdateInterface.on('update-downloaded', (updateInfo) => {
	if (electronRenderer && electronRenderer.store) {
		if (updateInfo && updateInfo.forceUpdate) {
			electronRenderer.store.dispatch({type: 'modal/SHOW_MODAL', name: 'FORCE_ELECTRON_UPDATE', disableClosingAction: true, data: {}});
		} else {
			electronRenderer.store.dispatch({
				type: 'components/StatusBar/STATUS_BAR_OPEN',
				data: {
					type: 'restart'
				}
			});
		}
	} else {
		log.error('electronRenderer is not ready for update signal');
	}
});

interfaces.ConnectivityInterface.on('connectivity-available', () => {
	if (electronRenderer && electronRenderer.store) {
		electronRenderer.store.dispatch({type: 'components/StatusBar/STATUS_BAR_CLOSE'});
	} else {
		log.info('electronRenderer is not ready for signalling available connectivity');
	}
});

interfaces.ConnectivityInterface.on('connectivity-not-available', () => {
	if (electronRenderer && electronRenderer.store) {
		electronRenderer.store.dispatch({
			type: 'components/StatusBar/STATUS_BAR_OPEN',
			data: {
				type: 'offline'
			}
		});
	} else {
		log.info('electronRenderer is not ready for signalling not available connectivity');
	}
});

document.addEventListener('DOMContentLoaded', () => {
	if (window.Events) {
		window.Events.subscribe(window.Events.player.repeat_changed, (event, value) => {
			interfaces.DzPlayerInterface.emit('repeat-changed', value);
		});
		window.Events.subscribe(window.Events.player.shuffle_changed, (event, value) => {
			interfaces.DzPlayerInterface.emit('shuffle-changed', value);
		});
		window.Events.subscribe(window.Events.player.playing, (event, value) => {
			interfaces.DzPlayerInterface.emit('playing-changed', value);
		});
		window.Events.subscribe(window.Events.player.displayCurrentSong, (event, value) => {
			const title = value.SNG_TITLE || '';
			const artist = value.ART_NAME || '';
			const album = value.ALB_TITLE || '';

			const hostConfig = electronRenderer.config
				? electronRenderer.config.get('host_img_url_ssl')
				: 'images.deezer.com/images';

			const domain = `https://${hostConfig}`;
			const coverType = 'cover';
			const md5 = value.ALB_PICTURE || 'd41d8cd98f00b204e9800998ecf8427e';
			const height = 250;
			const width = 250;
			const color = '000000';
			const quality = 80;
			const ext = 'jpg';

			const cover = `${domain}/${coverType}/${md5}/${height}x${width}-${color}-${quality}-0-0.${ext}`;
			interfaces.DzPlayerInterface.emit('current-track-changed', {title, artist, album, cover});
		});
	}
});

export default electronRenderer;
